```
       Defender 
       inf cav arty
inf     0  -1  -1 
cav    +2  +1  -1
arty 1  0   0   0
     2 -2  -1  -1
```

- Attacker is only inf 
  - Defender is only inf -> 0
  - Defender is cav and/or arty -> -1
- Attacker is only cav
  - Defender has arty -> -1 (including inf+arty,inf+cav+arty)
  - Defender has cav -> +1  (including inf+cav)
  - Defender is only inf -> +2
- Attacker is inf+cav
  - Defender has arty -> -1
  - Defender is inf+cav -> 0
  - Defender is only cav -> +1
  - Defender is only inf -> +2

If defender has arty, always -1, so 

- Defender has arty -> -1
- else 
  -> Defender has cav 
    - Attacker has cav 
      - Defender has inf and Attacker has inf -> 0
      - else -> +1
    - else -> -1
  - else
    - Attacker has cav -> +2
    - else -> 0
