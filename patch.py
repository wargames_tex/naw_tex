#
# TODO:
#
#   Check advanced VP, setup, etc. 
#
# 
from wgexport import *
from pprint import pprint


moreHelp = '''
<html>
 <head>
  <title>{title}</title>
  <style>
  </style>
 </head>
 <body>
  <h1>{title}</h1>
  <h2>Content</h2>
  <ul>
   <li><a href="#rules">Rules</a></li>
   <li><a href="#setup">Game or variant</a></li>
   <li><a href="#turn">Turn and phase tracker</a></li>
   <li><a href="#reinf">Reinforcements</a></li>
   <li><a href="#battle">Battle declaration and resolution</a></li>
   <li><a href="#vp">Victory points and conditions</a></li>
   <li><a href="#tut">Tutorial</a></li>
   <li><a href="#about">About this game</a></li>
   <li><a href="#credits">Credits</a></li>
  </ul>
  <h2><a name="rules"></a>Rules</h2>
  <p>
    A PDF of the rules is available through the <b>Help</b> menu.
    Please note that it may take a little while for your PDF viewer
    to launch
  </p>
  <h2><a name="setup"></a>Game or variant</h2>

  <p>Before getting to the game interface, you will be prompted to
   select a board.  There are two possibilities:</p>

  <dl>
   <dt>Small</dt>
   <dd>This is the default board for the <i>Basic game</i> and its
    variants. Reinforcements will arrive a turn later when playing the
    <i>Advanced game</i> with this board. This board is recommended if
    you want to play the <i>Basic game</i> or one of its
    variants. </dd>
   <dt>Large</dt>
   <dd>This is the default board for the <i>Advanced game</i>. This
    board has one more hex column to the west, 3 to the east, and 5
    more hex columns to the south.  This means that reinforcements will
    arrive a turn earlier when playing the <i>Basic game</i> and its
    variants.  This is the recommended map if you want to play the
    <i>Advanced game</i>.</dd>
  </dl>

  <p> At the start of a new game, you will be presented with a window
   to select which game or variant you want to play. You will
   <i>not</i> be able to change this later in the game.
   </p>

  <p>The possible games and variants are:</p>
  <dl>
   <dt>Basic</dt>
   <dd>The basic game. This is a relatively simple game with division
     (xx) sized units. This is an excellent introductory game for
     wargame beginners.</dd>
   <dt>Grouchy</dt>
   <dd>This is a variant of the <i>Basic game</i>.  In this variant,
    some randomness and fog-of-war is introduced by having random
    reinforcement schedules.  That is, in this variant, Field Marshall
    Grouchy <i>may</i> come to the aid of Napoleon, and the Field
    Marshall Bl&uuml;cher <i>may</i> reinforce the British
    faction.</dd>
  <dt>Esdaile</dt>
  <dd>This variant of the <i>Basic game</i> is based on an article by
   Professor Emeritus Charles J. Esdaile in <i>Journal of Advanced
   Military Studies</i>, <b>Vol.12</b>, issue 2, pages 11 to 14.  This
   variant introduces some realism into the game while maintaining the
   simplicity of the game.  The variant is slightly biased towards an
   Allied victory.</dd>
  <dt>Advanced</dt>
  <dd>This game is a more complex game with brigade (x) sized units.
    Artillery is handled in separate phases, and there are more units
    on the map.  Factions can choose tactics in combats (though the
    module will automatically select the optimal tactic for you).
    This is best played on the <i>Large</i> map.  </dd>
  </dl>

  <p> All units are placed in their setup locations.
  </p>

  <p> When the game is progressed to the first turn <b>French
   movement</b> phase, then game pieces that are not in used are
   removed and the game or variant is locked.  </p>

  <p> I you play by email, then one of you should select the game or
   variant, save the game <i> before</i> moving to the <b>French
   movement</b> phase, and send it off to the other side for
   validation. Only then should you progress to the next phase.  </p>

  <h2><a name="turn"></a>Turn and phase tracker</h2>

  <p> Use the <b>Turn tracker</b> interface in the menubar.  A number
   of automatic actions are implemented in this module, which heavily
   depend on the use of that interface.  </p>

  <p> You can go to the next phase by pressing the <b>+</b> button, or
   by pressing <code>Alt-T</code>.  This is the <i>only</i> possible
   way of moving the turn marker.</p>

  <p><b>Note</b>: The die-roll pop-up windows may steal focus from the
   main window, which means that key-board short-cuts, such as
   <code>Alt-T</code> are not registered.  Simply click the main
   window to bring that back into focus, and keyboard short cuts will
   be registered again.</p>

  <h2><a name="reinf"></a>Reinforcements</h2>

  <p>Reinforcement units arrive at the indicated turn on the edge of
    the board.  The factions must move them on to the board themselves
    (remember to account for spend MFs).  </p>

  <p>The <i>Grouchy variant</i> uses a random reinforcement schedule
   for both factions.  This is automatically handled by the module.
   The module will place a face-down reinforcement chit on each
   factions OOB.  The owning faction may click it to reveal the
   schedule, but the other faction cannot click it, nor see its value
   when switch to the that player.</p>

  <h2><img src="battle-marker-icon.png" width=24 height=24/><a
   name="battle"></a> &nbsp;Battle declaration and resolution</h2>

  <p>Battles <i>should</i> be declared in a factions <b>movement</b>
    phase.</p>

  <p>The module can automatically calculate odds and resolve combats
    via the use of <i>battle markers</i>.  To place a battle marker,
    select the attacking <i>and</i> defending units and press the
    battle marker button (<img src="battle-marker-icon.png"/>) or
    <code>Ctrl-X</code>.  This will place markers on the invovled
    units that identifiy the battle uniquely.</p>

  <p>If the user preference <b>Calculate Odds on battle
    declaration</b> is enabled (default), then the module will also
    calculate the odds (inluding bonuses and penalties for terrain),
    and place an odds marker (e.g., <img width=24 heigh=24
    src="odds_marker_2:1.png"/>) on one of the involved units.</p>

  <p>The module takes into account terrain of the defender, etc..  In
    the <i>Advanced game</i>, the opt.imal tactic for each faction is
    automatically chosen.  <b>Important</b>: ranged bombardment does
    not have the DRM automatically calculated.  It is up to the users
    to adjust the results accordingly by looking up the DRM modified
    result in the CRT.  </p>

  <p>Odds can be reduced by right clicking the odds marker and
    selecting <b>Reduce</b> (or by <code>Ctrl-L</code>).  Multiple
    reductions are possible by repeated use of that command.</p>

  <p>If a faction becomes <i>demoralised</i>, then all remaining
    combat odds are automatically recalculated by the module, if the
    preference <b>Calculate Odds on battle.  declaration</b> is
    enabled.  Odds can be recalculated at any time by right clicking
    an odds marker and selecting <i>Recalculate</i> or
    <code>Cltr-Shift-X</code>.</p>

  <p>If the user preference <b>Resolve battle results
    automatically</b> is enabled (default), then in the factions
    <b>Combat</b> phase, you can right click the odds marker and
    select <b>Resolve</b> (or <code>Ctrl-Y</code> or <img
    src="resolve-battles-icon.png" width=24 height=24> button) to
    resolve the combat.  The module will roll the dice and do the
    appropriate calculations, and a replace the odds marker with
    a <i>result marker</i> (e.g. <img width=24 height=24
    src="result_marker_DR.png"/>, for example).</p>

  <p>The module will <i>not</i> apply the result to the invovled
    units. This <i>must</i> be done, <i>according to the rules</i>, by
    the factions.  If a unit is eliminated as a consequence of the
    combat, then the short cut <code>Ctrl-E</code> or eliminate button
    <img src="eliminate-icon.png" width=24 height=24> can be used.
    The unit is then moved to the factions dead unit pool.</p>

  <p>To clear a battle declaration, select one of the involved units
    or battle markers, and select <b>Clear</b> (or press
    <code>Ctrl-C</code>) from the context menu. All battles can be
    cleared by the <img src="clear-battles-icon.png"/> button in the
    menubar.</p>

  <h2><a name="vp"></a>Victory points and conditions</h2>

  <p>The module automatically keeps track of victory conditions.</p>

  <p><i>Do not</i> use the dead pool for units that left the map
   <i>voluntarily</i>.  Those will <i>not</i> count towards a factions
   VPs. Instead, exited French units should be placed in the dedicated
   box on the board.</p>

  <p>When a unit is eliminated, the current victory conditions are
   checked automatically.  If a sudden death condition has been
   reached, then a pop-up window will announce the winner.  Note, in
   the <i>Advanced game</i> there are not sudden-death
   conditions. </p>

  <p>On turn "11", a pop-up will show the final results, if no
    sudden-death condition was reached.</p>

  <h2><a name="tut"></a>Tutorial</h2>

  <p>The tutorial of this module plays out a few turns of the <i>Basic
    Game</i>. </p>

  <p>To step through the tutorial, press <code>Page-down</code>. 
    On some steps you may need to press the key twice.</p>

  <h2><a name="about"></a>About this game</h2>

  <p> This VASSAL module was created from L<sup>A</sup>T<sub>E</sub>X
    sources of a Print'n'Play version of the <b>Napoleon at
    Waterloo</b> game.  That (a PDF) can be found at </p>

  <center>
    <code>https://gitlab.com/wargames_tex/naw_tex</code>
  </center>

  <p> where this module, and the sources, can also be found.  The PDF
   can also be found as the rules of this game, available in the
   <b>Help</b> menu.  </p>


  <h3>Change log</h3>
  <dl>
   <dt>1.1</dt>
   <dd>Fixed advanced game setup<br>
     Automatic recalculation of odds when faction demorialised<br>
     Button to show current victory status<br>
     Restrict certain commands based on phase<br>
  </dd>
   <dt>1.0</dt>
   <dd>Initial release of module</dd>
  </dl>

  <h2><a name="credits"></a>Credits</h2>
  <dl>
   <dt>Game design</dt>
   <dd>James F. Dunnigan</dd>
   <dt>Grouchy variant</dt>
   <dd>Albert A. Nof</dd>
   <dt>Esdaile variant</dt>
   <dd>Charles J. Esdaile</dd>
   <dt>Graphic design &amp; rules:</dt>
   <dd>Redmond A. Simonsen</dd>
   <dt>Editorial &amp; graphic production</dt>

   <dd>Rosalind Fructman, Ted Koller, Mike Moore, Manfred F. Milkuhn,
     &amp; Rob Ryer</dd>
  </dl>

  <h2>Copyright and license</h2>

  <p> This work is &#127279; 2024 Christian Holm Christensen, and
   licensed under the Creative Commons Attribution-ShareAlike 4.0
   International License. To view a copy of this license, visit </p>

  <center>
   <code>http://creativecommons.org/licenses/by-sa/4.0</code>
  </center>

  <p>
    or send a letter to
  </p>

  <center>
   Creative Commons<br>
   PO Box 1866<br>
   Mountain View<br>
   CA 94042<br>
   USA
  </center>
  
</body>
</html>'''

# --------------------------------------------------------------------
hexes = {
    '0110': 'houses',
    '0206': 'houses',
    '0208': 'houses',
    '0209': 'houses',
    '0309': 'houses',
    '0317': 'woods',
    '0421': 'woods',
    '0520': 'woods',
    '0521': 'woods',
    '0605': 'houses',
    '0606': 'houses',
    '0608': 'houses',
    '0620': 'woods',
    '0621': 'woods',
    '0708': 'houses',
    '0709': 'houses',
    '0804': 'woods',
    '0913': 'fortified',
    '0914': 'woods',
    '0920': 'woods',
    '0921': 'woods',
    '0922': 'woods',
    '1001': 'woods',
    '1002': 'houses',
    '1003': 'houses',
    '1013': 'woods',
    '1020': 'woods',
    '1021': 'woods',
    '1101': 'woods',
    '1106': 'houses',
    '1206': 'houses',
    '1310': 'fortified',
    '1406': 'woods',
    '1419': 'houses',
    '1502': 'woods',
    '1503': 'woods',
    '1601': 'woods',
    '1602': 'woods',
    '1617': 'houses',
    '1702': 'woods',
    '1711': 'fortified',
    '1716': 'houses', 
    '1717': 'houses',
    '1720': 'woods',
    '1721': 'woods',
    '1722': 'woods',
    '1810': 'houses', 
    '1817': 'houses',
    '1818': 'woods',
    '1819': 'woods',
    '1901': 'houses',
    '1916': 'woods',
    '1918': 'woods',
    '2010': 'houses',
    '2019': 'woods',
    '2102': 'woods',
    '2111': 'houses',
    '2112': 'woods',
    '2116': 'woods',
    '2119': 'woods',
    '2201': 'woods',
    '2214': 'woods',
    '2215': 'woods',
    '2217': 'woods',
    '2301': 'woods',
    '2302': 'woods',
    '2304': 'woods',
    '2305': 'woods',
    '2306': 'woods',
    '2307': 'houses',
    '2310': 'woods',
    '2314': 'woods',
    '2315': 'woods',
    '2317': 'woods',
    '2404': 'woods',
    '2405': 'woods',
    '2416': 'woods',
    '2504': 'woods',
    '2507': 'woods',
    '2511': 'woods',
    '2512': 'woods',
    '2513': 'woods',
    '2517': 'houses',
    '2604': 'houses',
    '2609': 'woods',
    '2610': 'woods',
    '2611': 'woods',
    '2612': 'woods',
    '2616': 'houses',
    '2617': 'houses',
}
# --------------------------------------------------------------------
def concatHexes(hexes):
    return ':'+':'.join(hexes) + ':'
# --------------------------------------------------------------------
def getHexes(hexes,what):
    return [k[:2]+'@'+k[2:] for k,v in hexes.items() if what in v]
# --------------------------------------------------------------------
def getWoodsHexes(hexes):
    return concatHexes(getHexes(hexes,'woods'))
# --------------------------------------------------------------------
def getHousesHexes(hexes):
    return concatHexes(getHexes(hexes,'houses'))
# --------------------------------------------------------------------
def getFortifiedHexes(hexes):
    return concatHexes(getHexes(hexes,'fortified'))
# --------------------------------------------------------------------
def addTurnPrototypes(n,
                      turns,
                      main,
                      prototypesContainer,
                      phase,
                      marker='game turn chit',
                      zoneName='turns',
                      globalBoard='',
                      globalScenario='',
                      effectiveTurn=''):
    # ----------------------------------------------------------------
    # Loop over turns, and create prototype for game turn marker to
    # move it along the turn track.  We do that by adding commands to
    # the turn track, which then delegates to the prototype.
    #
    # We add the prototypes to the game turn marker later on.
    board = main['mapName']
    turnp = []
    for t in range(1,n+1):
        k = key(NONE,0)+f',Turn{t}'
        r = f'Turn {t}@{zoneName}'
        turns.addHotkey(hotkey       = k,
                        match        = (f'{{Turn=={t}&&Phase=="{phase}"}}'),
                        reportFormat = f'=== <b>Turn {t}</b> ===',
                        name         = f'Turn {t}')
        main.addMassKey(name         = f'Turn marker to {t}',
                        buttonHotkey = k,
                        hotkey       = k,
                        buttonText   = '', 
                        target       = '',
                        filter       = (f'{{BasicName == "{marker}"}}'),
                        reportFormat = (f'{{wgDebug?"Move to turn {t}":""}}'))
        pn     = f'To turn {t}'
        traits = [
            TriggerTrait(
                name       = '',
                command    = 'Update turn',
                key        = k,
                property   = '',
                actionKeys = [k+'Move',k+'Update']),
            # f'{{IsThisScenario&&InTurn&&EffectiveTurn==Turn&&IsScheduled}}',
            SendtoTrait(mapName     = main['mapName'],
                        boardName   = '', #board,
                        name        = '',
                        restoreName = '',
                        restoreKey  = '',
                        zone        = zoneName,
                        destination = 'R', #R for region
                        region      = r,
                        key         = k+'Move',
                        x           = 0,
                        y           = 0),
            GlobalPropertyTrait(
                ['',
                 f'{k}Update',GlobalPropertyTrait.DIRECT,
                 f'{{{t}+'
                 f'({globalBoard}=="Large"?({globalScenario}==4?0:1):'
                 f'({globalBoard}=="Small"?({globalScenario}==4?-1:0):0))'
                 f'}}'
                 ],
                name        = effectiveTurn,
                numeric     = True,
                description = 'Update current effective turn'),
            ReportTrait(
                k,
                report = (f'{{wgDebug?("Moving "+BasicName+" to {r} "+{effectiveTurn}):""}}')),
            BasicTrait()]
        prototypesContainer.addPrototype(name        = f'{pn} prototype',
                                         description = f'{pn} prototype',
                                         traits      = traits)
        turnp.append(pn)

    return turnp

# --------------------------------------------------------------------
def prettyName(name,verbose=False):
    nations = {'fr': 'French',
               'br': 'British',
               'pr': 'Prussian'}
    echelons = {'b': 'Brigade',
                'd': 'Division',
                'r': 'Regiment'}
    types    = {'i': 'Infantry',
                'c': 'Cavalry',
                'ar': 'Artillery' }
    parts   = name.split(' ')
    if verbose: print(f'{name:<20s}',end='')
    #print(f'{name:<20s} -> {",".join(parts):<20s} ',end='')
    nation  = nations.get(parts.pop(0),'')
    
    if parts[-1] == '2':  parts.pop()
    last    = parts.pop()

    def squeeze(ret):
        s = ret.replace('  ',' ')
        if verbose: print(s)
        return s

    if last == 'nap':
        return squeeze('Napoleon')

    
    echelon = echelons.get(last[-1],'')
    main    = types.get(last[:-1],'')
    corps   = parts.pop(0).upper()
    if corps == 'REINF':
        return squeeze(name)
    if corps == 'RES':
        corps = 'Reserve'
    if corps == 'C':
        corps = 'Cavalry'
    if corps == 'GD':
        corps = 'Guard'
    if corps.endswith('C'):
        corps = corps[:-1]+' Cavalry'

    division = parts.pop(0) if len(parts) > 0 else ''
    brigade  = parts.pop(0) if len(parts) > 0 else ''
    regiment = parts.pop(0) if len(parts) > 0 else ''
    #fields   = [nation,corps,division, brigade,regiment,main,echelon]
    #print(f'{",".join(fields):<40s}',end='')

    if echelon == 'Regiment' and regiment == '':
        regiment = brigade
        brigade  = division
        division = ''

    if echelon == 'Brigade' and brigade == '':
        brigade = division
        division = ''
        regiment = ''
    
    div_map = { 'bw':   'Brunswick',
                'ha':   'Hanoverian',
                'hvy':  'Heavy',
                'lt':   'Light',
                'gen':  'General',
                'lf':   'Left flank',
                'rf':   'Right flank',
                'det':  'detachment',
                'c':    'Cavalry',
                'gren': 'Grenadier',
                'cha':  'Chasseur',
                'yng':  'Young' }
    post = {'db':  ' Dutch',
            'gd':  ' Guards',
            'kgl': ' King\'s German Legion',
            'ha':  ' Hanoverian',
            'hvy': ' Heavy',
            'det': ' Detachment',
            'in':  ' Indian',
            'bw':  ' Brunswick',
            'n':   ' Neumark',
            'p':   ' Pommern',
            's':   ' Schlesien',
            'w':   ' Westfalen',
            'e':   ' Elbe'}
                
                
    division = div_map.get(division,division)

    def map_post(what,post=post):
        for k,p in post.items():
            if what.endswith(k):
                return what[:-len(k)]+p
        return what

    def retnow(what,main,echelon):
        if what == main:
            return ' '+echelon
        return ' '+main+' '+echelon

    def numify(what):
        suf = {1: 'st',
               2: 'nd',
               3: 'rd',
               21: 'st',
               22: 'nd',
               23: 'rd',
               31: 'st',
               32: 'nd',
               33: 'rd',
               }
        p = what.split(' ')
        try:
            n = int(p[0])
            p[0] = f'{n}{suf.get(n,"th")}'
        except:
            pass
        return ' '.join(p)

    division = numify(map_post(division))
    brigade  = numify(map_post(brigade))
    regiment = numify(map_post(regiment))
    if brigade == division:
        division = ''

    ret = f'{nation} {corps} Corps, '

    if division != '':
        ret += f'{division}'
        if echelon == 'Division':
            return squeeze(ret + retnow(division,main,echelon))
        ret += ' Division, '

    if brigade != '':
        ret += f'{brigade}'
        if echelon == 'Brigade':
            return squeeze(ret+retnow(brigade,main,echelon))
        ret += ' Brigade, '

    return squeeze(ret + f'{regiment} {main} {echelon}')
 
# --------------------------------------------------------------------
# Our main patching function
def patch(build,data,vmod,gverbose=False):
    from re  import sub
    from PIL import Image
    from io  import BytesIO

    # --- Get real image name ----------------------------------------
    fileMapping    = vmod.getFileMapping()
    def getImage(name,mapping=fileMapping):
        from pathlib import Path
        
        real = mapping.get(Path(name).stem,None)
        if real is None:
            print(f'File {name} not found!')
            return None

        return Path(real).name
    
    game = build.getGame()

    # --- Global properties ------------------------------------------
    debug           = 'wgDebug'
    verbose         = 'wgVerbose'
    hidden          = 'wg hidden unit'
    battleUnit      = 'wgBattleUnit'
    battleCalc      = 'wgBattleCalc'
    battleCtrl      = 'wgBattleCtlr'
    battleNo        = 'wgBattleNo'
    battleShift     = 'wgBattleShift'
    currentBattle   = 'wgCurrentBattle'
    oddsProto       = 'OddsMarkers prototype'
    globalFortified = 'FortressHexes'
    globalWoods     = 'WoodsHexes'
    globalHouses    = 'HousesHexes'
    globalDefender  = 'DefenderHex'
    globalCombat    = 'NotCombat'
    globalScenario  = 'Scenario'
    globalVictor    = 'Victor'
    effectiveTurn   = 'ReinforcementTurn'
    isTutorial      = 'IsTutorial'
    globalBoard     = 'SelectedBoard'
    moraleLimit     = 'MoraleLimit'
    notCombat       = 'NotCombat'
    frenchExited    = 'FrenchExited'
    woodsHexes      = getWoodsHexes    (hexes)
    housesHexes     = getHousesHexes    (hexes)
    fortifiedHexes  = getFortifiedHexes(hexes)
    otherDemorale   = 'OpponentDemoralised'
    demorale        = 'Demoralised'
    defenderWoods   = 'DefenderWoods'
    attackerMorale  = 'AttackerDemorale'
    defenderMorale  = 'DefenderDemorale'
    gameTurn        = 'game turn chit'
    alliedVP        = 'AlliedVP'
    frenchVP        = 'FrenchVP'
    lastTurn        = 10
    battleDRM       = 'wgBattleDRM'
    scenarios       = ['Basic','Grouchy','Esdaile','Advanced']

    
    # --- Internal keys ----------------------------------------------
    initBoard      = key(NONE,0)+',initBoard'
    nextPhase      = key('T',ALT) # key(NONE,0)+',nextPhase'    
    resolveKey     = key('Y')
    setBattle      = key(NONE,0)+',wgSetBattle'
    dieRoll        = key(NONE,0)+',dieRoll'
    hideReinf      = key(NONE,0)+',hideReinf'
    setupKey       = key(NONE,0)+',setup'
    reinforceKey   = key(NONE,0)+',reinforce'
    scheduleKey    = key(NONE,0)+',schedule'
    optKey         = key('Q',ALT)
    deleteKey      = key(NONE,0)+',delete'
    setitupKey     = key(NONE,0)+',setitup'
    eliminateCmd   = key(NONE,0)+',eliminate'
    incrLossKey    = key(NONE,0)+',incrLoss'
    printKey       = key(NONE,0)+',print'
    tutorialKey    = key(NONE,0)+',isTutorial'
    storeMorale    = key(NONE,0)+',storeMorale'
    checkMorale    = key(NONE,0)+',checkMorale'
    updateMorale   = key(NONE,0)+',updateMorale'
    checkExit      = key(NONE,0)+',checkExit'
    checkVictory   = key(NONE,0)+',checkVictory'
    checkCombat    = key(NONE,0)+',checkCombat'
    checkSudden    = key(NONE,0)+',checkSudden'
    showWinner     = key(NONE,0)+',showWinner'
    updateType     = key(NONE,0)+',updateType'
    updateDRM      = key(NONE,0)+',updateDRM'
    updateWoods    = key(NONE,0)+',updateWoods'
    updateDemorale = key(NONE,0)+',updateDemorale'
    recalcAll      = key(NONE,0)+',recalcAll'
    recalcOdds     = key('X',CTRL_SHIFT)
    
    # ----------------------------------------------------------------
    # Extra documentation 
    doc  = game.getDocumentation()[0]
    more = doc.addHelpFile(title='More information',fileName='help/more.html')
    vmod.addFile('help/more.html', moreHelp.format(title=game['name']))

    # --- Global properties ------------------------------------------
    gp              = game.getGlobalProperties()[0];
    for h,n in [[woodsHexes,        globalWoods     ],   
                [housesHexes,       globalHouses    ],   
                [fortifiedHexes,    globalFortified ],   
                ['',                globalDefender  ],
                ['false',           isTutorial      ],
                ['',                globalBoard     ],
                ]:
        gp.addProperty(name         = n,
                       initialValue = h,
                       description  = f'List of {n} hexes')
    gp.addProperty(name         = globalScenario,
                   initialValue = 1,
                   description  = 'Select setup scenario')
    gp.addProperty(name         = effectiveTurn,
                   initialValue = 1,
                   description  = 'Reinforcement turn')
    gp.addProperty(name         = moraleLimit,
                   initialValue = 40,
                   description  = 'Demoralisation limit')
    gp.addProperty(name         = frenchExited,
                   initialValue = 0,
                   description  = 'Exited French units')
    gp.addProperty(name         = globalVictor,
                   initialValue = 'Tie',
                   description  = 'Winner')
    gp.addProperty(name         = defenderWoods,
                   initialValue = 'false',
                   description  = 'Some defender in woods')
    gp.addProperty(name         = defenderMorale,
                   initialValue = 'false',
                   description  = 'Defender demoralised')
    gp.addProperty(name         = attackerMorale,
                   initialValue = 'false',
                   description  = 'Attacker demoralised')
    gp.addProperty(name         = battleDRM,
                   initialValue = 0,
                   description  = 'Die roll modifier')
    gp.addProperty(name         = notCombat,
                   initialValue = True,
                   description  = 'True if not combat/bombardment phase')

    for f in ['Allied','French']:
        for v,s,t in zip(['Schedule', 'Loss', 'Morale','Old','VP'],
                         [1,          0,      1,        1,   0],
                         ['reinforcement schedule',
                          'total CF loss',
                          'demoralisation',
                          'old demoralisation',
                          'victory points']):
            gp.addProperty(name         = f+v,
                           initialValue = s,
                           description  = 'French faction '+t)
    for ptype in ['infantry', 'reconnaissance', 'artillery']:
        avar   = ptype.capitalize()+'Attacker'
        dvar   = ptype.capitalize()+'Defender'
        gp.addProperty(name         = avar,
                       initialValue = 'false',
                       description  = f'{ptype} in attackers')
        gp.addProperty(name         = dvar,
                       initialValue = 'false',
                       description  = f'{ptype} in defenders')
    
    # --- Names of phases --------------------------------------------
    phaseNames     = ['Setup',
                      'French movement',
                      'French bombardment',
                      'French combat',
                      'Allied movement',
                      'Allied bombardment',
                      'Allied combat']
    setupPhase = phaseNames[0]
    frMove     = phaseNames[1]
    frBomb     = phaseNames[2]
    frCombat   = phaseNames[3]
    brMove     = phaseNames[4]
    brBomb     = phaseNames[5]
    bfCombat   = phaseNames[6]

    # ----------------------------------------------------------------
    # Get the turns interface, and set up.  We built a set of traits
    # to add to the game turn marker to progress the turn track.
    turns                 = game.getTurnTracks()['Turn']
    turns['reportFormat'] = '{"<b>Phase</b>:  <u>"+Phase+"</u>"}'
    phases                = turns.getLists()['Phase']
    phases['list']        = ','.join(phaseNames)
    
    keys = turns.getHotkeys(asdict=True)
    keys['Clear battle markers']['match'] = (
        f'{{Phase=="{frMove}"'
        f'||Phase=="{brMove}"}}')

    # ----------------------------------------------------------------
    # CRTs       odds    1    2    3    4    5    6
    basicCrt = { '1:5': ['AE','AE','AE','AE','AE','AE'],
                 '1:4': ['AR','AE','AE','AE','AE','AE'],
                 '1:3': ['AR','AR','AE','AE','AE','AE'],
                 '1:2': ['DR','AR','AR','AR','AR','AE'],
                 '1:1': ['DR','DR','DR','AR','AR','AR'],
                 '2:1': ['EX','DR','DR','DR','DR','AR'],
                 '3:1': ['DE','EX','DR','DR','DR','DR'],
                 '4:1': ['DE','DE','EX','EX','DR','DR'],
                 '5:1': ['DE','DE','DE','DE','EX','EX'],
                 '6:1': ['DE','DE','DE','DE','DE','DE'] }
    #             odds   -1   0.   1    2    3    4    5    6    7    8
    advCrt   = { '1:5': ['AE','AE','AE','AE','AE','AE','AE','AE','AE','AR'],
                 '1:4': ['AE','AE','AE','AE','AE','AE','AE','AE','AE','AR'],
                 '1:3': ['AE','AE','AE','AE','AE','AE','AE','AR','AR','AR'],
                 '1:2': ['AE','AE','AE','AR','AR','AR','AR','DR','DR','DR'],
                 '1:1': ['AE','AE','AR','AR','AR','DR','DR','DR','DR','EX'],
                 '2:1': ['AE','AR','AR','DR','DR','DR','DR','EX','EX','DE'],
                 '3:1': ['AR','AR','DR','DR','DR','DR','EX','DE','DE','DE'],
                 '4:1': ['DR','DR','DR','DR','EX','EX','DE','DE','DE','DE'],
                 '5:1': ['DR','EX','EX','EX','DE','DE','DE','DE','DE','DE'],
                 '6:1': ['EX','EX','DE','DE','DE','DE','DE','DE','DE','DE'] }
    # ----------------------------------------------------------------
    # Dice
    diceName  = '1d6Dice'
    diceKey   = key('6',ALT)
    dices     = game.getSymbolicDices()
    for dn, dice in dices.items():
        if dn == diceName:
            dice['hotkey'] = diceKey
            dice['icon']   = getImage('d6-icon')

    game.getPieceWindows()['Counters']['icon'] = getImage('unit-icon')
        
    # ----------------------------------------------------------------
    prototypeContainer = game.getPrototypes()[0]
    # ----------------------------------------------------------------
    maps  = game.getMaps()
    main  = maps['Board']
    ostart = main.getAtStarts(False)
    for at in ostart:
        if at['name'] == hidden:
            at["owningBoard"] = '';

    # ----------------------------------------------------------------
    # Delete key
    # pprint(main.getMassKeys())
    dkey = main.getMassKeys().get('Delete',None)
    if dkey is not None:
        dkey['icon'] = ''
    fkey = main.getMassKeys().get('Flip',None)
    if fkey is not None:
        fkey['icon'] = ''
    rkey = main.getMassKeys().get('Selected resolve battle',None)
    if rkey is not None:
        rkey['canDisable']   = True
        rkey['propertyGate'] = notCombat
    ekey = main.getMassKeys().get('Eliminate',None)
    if ekey is not None:
        ekey['canDisable']   = True
        ekey['propertyGate'] = notCombat

    # Reduce odds
    main.addMassKey(name         = 'Reduce odds',
                    buttonHotkey = key('L'),
                    hotkey       = key('L'),
                    buttonText   = '', 
                    singleMap    = True,
                    tooltip      = 'Reduce combat odds one level',
                    reportFormat = (f'{{{verbose}?("Reduce odds"):""}}'))

    # Recalc odds
    main.addMassKey(name         = 'Recalculate odds',
                    buttonHotkey = recalcAll,
                    hotkey       = recalcOdds,
                    buttonText   = '', 
                    singleMap    = True,
                    tooltip      = '',
                    target       = '',
                    filter       = f'{{wgOddsMarker==true}}',
                    reportFormat = (f'{{{verbose}?("Recalculate odds"):""}}'))
    
    # ----------------------------------------------------------------
    # Remove dead-pool
    if 'DeadMap' in maps: 
        game.remove(maps['DeadMap'])
    

    # ----------------------------------------------------------------
    # Add layers to the map
    pieceLayer      = 'PieceLayer'
    unitLayer       = 'Units'
    btlLayer        = 'Units' #'Battle' - doesn't work with auto odds
                              #if not same as units - anyways, the
                              #markers should stack with units.
    oddLayer        = 'Odds'
    resLayer        = 'Result'
    layerNames      = {unitLayer: {'t': unitLayer, 'i': '' },
                       #btlLayer,
                       oddLayer: {'t': oddLayer+' markers', 'i': ''},
                       resLayer: {'t': resLayer+' markers', 'i': ''} }

    layers = main.addLayers(description='Layers',
                             layerOrder=layerNames)

    for lt,ln in layerNames.items():
        layers.addControl(name       = lt,
                          tooltip    = f'Toggle display of {ln}',
                          text       = f'Toggle {ln["t"]}',
                          icon       = ln['i'],
                          layers     = [lt])
    layers.addControl(name    = 'Show all',
                      tooltip = f'Show all',
                      text    = f'Show all',
                      command = LayerControl.ENABLE,
                      layers  = list(layerNames.keys()))

    lmenu = main.addMenu(description = 'Toggle layers',
                         text        = '',
                         tooltip     = 'Toggle display of layers',
                         icon        = getImage('layer-icon'),
                         menuItems   = ([f'Toggle {ln["t"]}'
                                            for ln in layerNames.values()]
                                            +['Show all']))
    hideP  = main.getHidePiecesButton()
    if len(hideP) > 0:
        # print('Found hide pieces button, moving layers before that')
        main.remove(layers)
        main.remove(lmenu)
        main.insertBefore(layers,hideP[0])
        main.insertBefore(lmenu, hideP[0])
    else:
        # print('Hide pieces button not found',hideP)
        pass 

    # ----------------------------------------------------------------
    game.addStartupMassKey(name        = 'Selected board',
                           hotkey      = initBoard,
                           target      = '',
                           singleMap   = False,
                           filter      = f'{{BasicName=="{hidden}"}}',
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("~ Selected board \'"+'
                           f'{globalBoard}+"\'"+" \'"+CurrentBoard+"\'"):""}}')
    
    
    # ----------------------------------------------------------------
    # Get optionals map
    opts = maps['Optionals']
    opts['allowMultiple'] = False
    opts['markMoved']     = 'Never'
    opts['hotkey']        = optKey
    opts['launch']        = True
    opts['icon']          = getImage('opt-icon')
    opts.remove(opts.getImageSaver()[0])
    opts.remove(opts.getTextSaver()[0])
    opts.remove(opts.getGlobalMap()[0])
    opts.remove(opts.getHidePiecesButton()[0])
    obrd  = opts.getBoardPicker()[0].getBoards()['Optionals']
    okeys = opts.getMassKeys()
    for kn,k in okeys.items():
        opts.remove(k) 
    ostart = opts.getAtStarts(False)
    for at in ostart:
        if at['name'] == hidden:
            opts.remove(at)
    
    game.addStartupMassKey(name        = 'Optionals',
                           hotkey      = optKey,
                           target      = '',
                           filter      = f'{{BasicName=="{hidden}"}}',
                           singleMap   = False,
                           whenToApply = StartupMassKey.START_GAME,
                           reportFormat=
                           f'{{{debug}?("~ Show optionals window"):""}}')

    # ----------------------------------------------------------------
    # Modify the detail viewer
    # #dddda4
    boardColor                    = rgb(0xdd,0xdd,0xa4)
    locFormat                     = \
        '{"<b>"+LocationName.replace("@","")+"</b>"}'
    viewer                        = main.getCounterDetailViewer(True)[0]
    viewer['bgColor']             = boardColor # Board background
    viewer['borderWidth']         = 2
    viewer['graphicsZoom']        = 1
    viewer['summaryReportFormat'] = locFormat
    viewer['emptyHexReportForma'] = locFormat
    main.addCounterDetailViewer(
        fontSize            = 16,
        bgColor             = boardColor,
        borderWidth         = 0,
        delay               = 1000,
        summaryReportFormat = locFormat,
        emptyHexReportForma = locFormat,
        hotkey              = key('\n'),
        showgraph           = False,
        minDisplayPieces    = 0)

    
    # ----------------------------------------------------------------
    # Move large board into main picker
    largeMap   = maps['Large']
    largeBoard = largeMap.getBoardPicker()[0].getBoards()['Large']

    # ----------------------------------------------------------------
    # Get Zoned area
    picker      = main.getBoardPicker()[0]
    smallBoard  = picker.getBoards()['Board']
    zoned       = smallBoard.getZonedGrids()[0]
    # Add Large map to main picker
    picker.append(largeBoard)
    game.remove(largeMap)
    smallBoard['name'] = 'Small'
    
    # high  = zoned.getHighlighters()[0]
    # 
    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hnum   = hgrid.getNumbering()[0]
    # # print(f'grid offsets: h={hnum["hOff"]} v={hnum["vOff"]}')
    # # hnum['hOff']  =  0
    hnum['vOff']  = int(hnum['vOff']) - 1
    hnum['sep']   = '@'

    # ----------------------------------------------------------------
    # Adjust the hexes
    #
    # Get the hex grid and turn of drawing
    zoned  = largeBoard.getZonedGrids()[0]
    zones  = zoned.getZones()
    hzone  = zones['hexes']
    hgrids = hzone.getHexGrids()
    hgrid  = hgrids[0]
    hgrid['y0'] = 116
    hnum   = hgrid.getNumbering()[0]
    # # print(f'grid offsets: h={hnum["hOff"]} v={hnum["vOff"]}')
    # # hnum['hOff']  =  0
    hnum['vOff']  = int(hnum['vOff']) - 1
    hnum['hOff']  = int(hnum['hOff']) - 1
    hnum['stagger'] = True
    hnum['sep']   = '@'


    # Remove some grids
    for board in [smallBoard,largeBoard]:
        zoned = board.getZonedGrids()[0]
        zones = zoned.getZones()

        for zn in ['pr upmarch','fr upmarch','exit',
                   'French dead','Allied dead']:
            zone = zones.get(zn,None)
            if zone is None:
                print(f'Zone "{zn}" not found')
                continue

            grid = zone.getGrids()[0]
            # print(f'Removing grid from zone {zn}')
            zone.remove(grid)



    # ----------------------------------------------------------------
    # Specific actions 
    turns.addHotkey(hotkey = nextPhase,
                    match  = '{Phase=="Setup"&&Turn>1}',
                    name   = 'Skip setup phase on later turns')
    turns.addHotkey(hotkey = nextPhase,
                    match  = (f'{{Phase.contains("bombardment")&&'
                              f'{globalScenario}!=4}}'),
                    name   = 'Skip bombardment phase if not advanced')

    for phase in [frMove,brMove]:
        turns.addHotkey(hotkey       = reinforceKey,
                        match        = f'{{Turn!=1&&Phase=="{phase}"}}',
                        reportFormat = '',#f'--- <t>Reinforce</i> ---',
                        name         = f'Reinforce in phase={phase}')
        main.addMassKey(name         = f'Reinforce in phase={phase}',
                        buttonHotkey = reinforceKey,
                        hotkey       = reinforceKey,
                        buttonText   = '', 
                        target       = '',
                        singleMap    = False,
                        filter       = (f'{{CurrentBoard != {globalBoard}}}'),
                        reportFormat = (f'{{{debug}?"~ Reinforce in "+'
                                        f'"phase={phase} on board "+'
                                        f'{globalBoard}:""}}'))
    turns.addHotkey(hotkey       = setupKey,
                    match        = f'{{Turn==1&&Phase=="{frMove}"}}',
                    reportFormat = '',#f'--- <i>Setup</i> ---',
                    name         = f'Setup units')
    main.addMassKey(name         = f'Setup units',
                    buttonHotkey = setupKey,
                    hotkey       = setupKey,
                    buttonText   = '', 
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{CurrentBoard != {globalBoard}}}'),
                    reportFormat = (f'{{{debug}?"Setup units "+'
                                    f'{globalBoard}:""}}'))
    turns.addHotkey(hotkey       = setitupKey,
                    match        = f'{{Turn==1&&Phase=="{frMove}"}}',
                    reportFormat = '', #f'--- <t>Select schedules</i> ---',
                    name         = f'Select reinforcement schedules')
    main.addMassKey(name         = f'Select reinforcement schedules',
                    buttonHotkey = setitupKey,
                    hotkey       = setitupKey,
                    buttonText   = '', 
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{BasicName == "{hidden}"}}'),
                    reportFormat = (f'{{{debug}?"~ Settting up":""}}'))
        
    # ----------------------------------------------------------------
    turns.addHotkey(hotkey       = checkVictory,
                    match        = f'{{Turn>{lastTurn}}}',
                    reportFormat = '', #f'--- <t>Select schedules</i> ---',
                    name         = f'Check for victory at end of game')
    main.addMassKey(name         = 'Check for victory',
                    buttonHotkey = checkVictory,
                    hotkey       = checkVictory,
                    icon         = '',
                    buttonText   = '', 
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{BasicName == "{hidden}"}}'),
                    reportFormat = (f'{{{debug}?"~ Check victory":""}}'))
    main.addMassKey(name         = 'Check for victory',
                    buttonHotkey = key('V',ALT),
                    hotkey       = checkVictory+'User',
                    icon         = getImage('victory-icon'),
                    buttonText   = '', 
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{BasicName == "{hidden}"}}'),
                    reportFormat = (f'{{{debug}?"~ Check victory by user":""}}'))
    turns.addHotkey(hotkey       = checkCombat,
                    match        = '',
                    reportFormat = '', #f'--- <t>Select schedules</i> ---',
                    name         = f'Check for combat phases')
    main.addMassKey(name         = 'Check for combat phase',
                    buttonHotkey = checkCombat,
                    hotkey       = checkCombat,
                    buttonText   = '', #'V', 
                    target       = '',
                    singleMap    = False,
                    filter       = (f'{{BasicName == "{hidden}"}}'),
                    reportFormat = (f'{{{debug}?"~ Check for combat":""}}'))
    main.addMassKey(
        name         = 'Show winner',
        buttonHotkey = showWinner,
        hotkey       = showWinner+'More',
        buttonText   = '', 
        target       = '',
        singleMap    = False,
        filter       = (f'{{BasicName == "{hidden}"}}'),
        reportFormat = (f'{{(({globalVictor}!="Tie"&&{globalScenario}<4)'
                        f'||Turn>{lastTurn})?'
                        f'Alert("The battle is a "+{globalVictor}):""}}'))
                    
    # ----------------------------------------------------------------
    # main.addMassKey(
    #     name = 'Print board',
    #     buttonText = 'B',
    #     reportFormat = (f'{{{verbose}?("Board is "+{globalBoard}):""}}'))
    
    # ----------------------------------------------------------------
    # Turn track progress 
    turnp = addTurnPrototypes(10,turns,main,prototypeContainer,
                              frMove, #phaseNames[0],
                              marker=gameTurn,
                              zoneName='turns',
                              globalBoard=globalBoard,
                              globalScenario=globalScenario,
                              effectiveTurn=effectiveTurn
                              )

    # ----------------------------------------------------------------
    # Add some global keys to the map
    # ----------------------------------------------------------------
    curBtl        = (f'{{{battleNo}=={currentBattle}&&'
                    f'{battleUnit}==true}}')
    curDef        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==false}}')
    curAtt        = (f'{{{battleNo}=={currentBattle}&&'
                     f'{battleUnit}==true&&'
                     f'IsAttacker==true}}')
     
    # ================================================================
    #
    # Prototypes
    #
    # ----------------------------------------------------------------
    # Clean up prototypes
    prototypes         = prototypeContainer.getPrototypes(asdict=False)
    seen       = list()
    for p in prototypes:
        if p['name'] == ' prototype':
            prototypes.remove(p)
        if p['name'] in seen:
            # print(f'Removing prototype {p["name"]}')
            prototypes.remove(p)
        seen.append(p['name'])

    prototypes     = prototypeContainer.getPrototypes(asdict=True)

    # ----------------------------------------------------------------
    # Type prototypes
    for ptype in ['infantry', 'reconnaissance', 'artillery']:
        typep = prototypes.get(f'{ptype} prototype',None)
        if typep is None:
            raise f'Failed to find prototype for "{ptype}"'

        traits = typep.getTraits()
        basic  = traits.pop()
        avar   = ptype.capitalize()+'Attacker'
        dvar   = ptype.capitalize()+'Defender'
        traits.extend([
            TriggerTrait(
                name       = '',
                command    = '',
                key        = updateType,
                actionKeys = [updateType+'Attacker',
                              updateType+'Defender']),
            GlobalPropertyTrait(
                ['',updateType+'Attacker',GlobalPropertyTrait.DIRECT,
                 f'{{({avar}||!InTurn)?{avar}:InTurn}}'],
                name = avar,
                numeric = False,
                description = f'Set {ptype} attacker'),
            GlobalPropertyTrait(
                ['',updateType+'Defender',GlobalPropertyTrait.DIRECT,
                 f'{{({dvar}||InTurn)?{dvar}:!InTurn}}'],
                name = dvar,
                numeric = False,
                description = f'Set {ptype} defender'),
            ReportTrait(
                updateType+'Attacker',
                report = (f'{{{debug}?("~ Set {ptype} in attackers "+'
                          f'InTurn+" -> "+{avar}):""}}')),
            ReportTrait(
                updateType+'Defender',
                report = (f'{{{debug}?("~ Set {ptype} in defenders "+'
                          f'!InTurn+" -> "+{dvar}):""}}')),
        ])
        typep.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify Markers prototype
    markersP     = prototypes['Markers prototype']
    traits       = markersP.getTraits()
    mdel         = Trait.findTrait(traits,DeleteTrait.ID)
    mdel['key']  = deleteKey
    mdel['name'] = ''        
    markersP.setTraits(*traits)

    # ----------------------------------------------------------------
    # Modify odds prototype
    #  1: 1:5
    #  2: 1:4
    #  3: 1:3
    #  4: 1:2
    #  5: 1:1
    #  6: 2:1
    #  7: 3:1
    #  8: 4:1
    #  9: 5:1
    # 10: 6:1
    oddsP          = prototypes['OddsMarkers prototype']
    traits = oddsP.getTraits()
    basic  = traits.pop()
    res    = Trait.findTrait(traits,CalculatedTrait.ID,
                             key='name',value='BattleResult')
    dce    = Trait.findTrait(traits,CalculatedTrait.ID,
                             key='name',value='Die')
    trg    = Trait.findTrait(traits,TriggerTrait.ID,
                             key='key',value=resolveKey)
    rep    = None
    for t in traits:
        if t.ID != ReportTrait.ID: continue
        # print(f"- Report: {t['report']}")
        if 'Battle #' in t['report']:
            rep = t
            break
    # print(f'Report: {rep}')
    if rep is not None:
        rep['report'] = rep['report'].replace('Die+"','Die+" (DRM "+DRM+") ')\
            .replace('BattleDetails+','OddsName+')
        
    dce['expression'] = f'{{GetProperty("{diceName}_result")+DRM}}'
    res['expression'] = f'{{{globalScenario}==4?(AdvancedResult):(BasicResult)}}'
    traits.extend([
        MarkTrait(name = pieceLayer, value = oddLayer),
        DynamicPropertyTrait(
            ['',key(NONE,0)+',wgGetBattle',DynamicPropertyTrait.DIRECT,
             f'{{{battleDRM}}}'],
            name = 'DRM',
            numeric = True,
            description = 'Get DRM from global'),
        GlobalPropertyTrait(
            ['',key(NONE,0)+',wgSetBattle',GlobalPropertyTrait.DIRECT,'{DRM}'],
            name = battleDRM,
            numeric = True,
            description = 'Set DRM as global')])
             
    oddsP.setTraits(*traits,basic)

    # ----------------------------------------------------------------
    # Create prototypes for the different scenarios
    #
    traits = [
        TriggerTrait(
            name       = '',
            command    = '',
            key        = setupKey,
            property   = f'{{InScenario}}',
            actionKeys = [setupKey+'Real']),
        TriggerTrait(
            name       = '',
            command    = '',
            key        = setupKey,
            property   = f'{{!InScenario}}',
            actionKeys = [deleteKey]),
        ReportTrait(
            deleteKey,
            report = f'{{{debug}?("Removing "+BasicName):""}}'),
        ReportTrait(
            setupKey+'Real',
            report = (f'{{{debug}?("Moving "+BasicName+" to "+UnitHex):""}}')),
        TriggerTrait(
            name       = '',
            command    = '', #Reinforce',
            key        = reinforceKey,
            property   = (f'{{InScenario&&InTurn&&'
                          f'{effectiveTurn}==UnitTurn&&InSchedule}}'),
            actionKeys = [reinforceKey+'Real']),
        ReportTrait(
            reinforceKey+'Real',
            report = f'{{{verbose}?("! Reinforce with "+PrettyName):""}}'),
        BasicTrait()]
    prototypeContainer.addPrototype(name        = f'Reinforce prototype',
                                    description = f'Reinforce prototype',
                                    traits      = traits)

    traits = [
        TriggerTrait(
            name       = '',
            command    = '',
            key        = setupKey,
            property   = f'',
            actionKeys = [hideReinf]),
        ReportTrait(
            hideReinf,
            report = f'{{{debug}?("~ Hide reinforcement schedule"):""}}'),
        BasicTrait()]
    prototypeContainer.addPrototype(name        = f'Schedule prototype',
                                    description = f'Schedule prototype',
                                    traits      = traits)
    
    # ----------------------------------------------------------------
    # Modify marker prototype
    btlP   = prototypes['BattleMarkers prototype']
    traits = btlP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = btlLayer))
    btlP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Modify results prototype
    resP   = prototypes['ResultMarkers prototype']
    traits = resP.getTraits()
    basic  = traits.pop()
    traits.append(MarkTrait(name = pieceLayer, value = resLayer))
    resP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Battle unit prototype update with forest, fortress, overriver rules
    #
    # Need a bit more massage 
    battleUnitP = prototypes[battleUnit]
    traits      = battleUnitP.getTraits()
    basic       = traits.pop()
    oddsS       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    effAF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveAF')
    effDF       = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='EffectiveDF')
    calcAF      = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name',value='wgBattleAF')
    calcDF      = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name',value='wgBattleDF')
    afCmd       = calcAF.getCommands()[0]
    dfCmd       = calcDF.getCommands()[0]
    afKey       = afCmd[1]
    dfKey       = dfCmd[1]
    afCmd[1]    = afKey+'Real'
    dfCmd[1]    = dfKey+'Real'
    

    oddsS['expression'] = '0'
    effAF['expression'] = (f'{{CF*'
                           f'(({otherDemorale}&&{globalScenario}==4)?2:1)}}')
    effDF['expression'] = (f'{{DF*'
                           f'(({otherDemorale}&&{globalScenario}==4)?2:1)'
                           f'*((InWoods&&{globalScenario}!=4)?2:1)'
                           f'*((InFortified)?({globalScenario}==3?3:2):1)'
                           f'*(InHouses?2:1)'
                           f'}}')
    calcAF.setCommands([afCmd])
    calcDF.setCommands([dfCmd])
    
    #                      (f'{{(!IsAttacker)?(InFortified?-3:'
    #                       f'(InWoods||InHouses)?-2:InRough?-1:0):'
    #                       f'wgBattleShift}}')
    traits.extend([
        CalculatedTrait(
            name        = f'In{name}',
            expression  = f'{{{globalScenario}=={i+1}}}',
            description = 'Current scenario')
        for i,name in enumerate(scenarios)])
    traits.extend([
        CalculatedTrait(
            name        = 'InWoods',
            expression  = (f'{{{globalWoods}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in woods hex?'),
        CalculatedTrait(
            name        = 'InHouses',
            expression  = (f'{{{globalHouses}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in houses hex?'),
        CalculatedTrait(
            name        = 'InFortified',
            expression  = (f'{{{globalFortified}.contains(":"+'
                           f'LocationName+":")}}'),
            description = 'Defender in fortified hex?'),
        GlobalPropertyTrait(
            ['',updateDemorale+'Attacker',GlobalPropertyTrait.DIRECT,
             f'{{({attackerMorale}||!IsAttacker)?{attackerMorale}:Demoralised}}'],
            name = attackerMorale,
            numeric = False,
            description = 'Update if attacker is demoralised'),
        GlobalPropertyTrait(
            ['',updateDemorale+'Defender',GlobalPropertyTrait.DIRECT,
             f'{{({defenderMorale}||IsAttacker)?{defenderMorale}:Demoralised}}'],
            name = defenderMorale,
            numeric = False,
            description = 'Update if defender is demoralised'),
        GlobalPropertyTrait(
            ['',updateWoods,GlobalPropertyTrait.DIRECT,
             f'{{({defenderWoods}||IsAttacker)?{defenderWoods}:InWoods}}'],
            name = defenderWoods,
            numeric = False,
            description = 'Update if a defender is in woods'),
        ReportTrait(
            updateDemorale+'Defender',
            report = (f'{{{debug}?("~ Defender demoralised: "'
                      f'+{defenderMorale}):""}}')),
        ReportTrait(
            updateDemorale+'Attacker',
            report = (f'{{{debug}?("~ Attacker demoralised: "'
                      f'+{attackerMorale}):""}}')),
        ReportTrait(
            updateWoods,
            report = (f'{{{debug}?("~ Defender in woods: "'
                      f'+{defenderWoods}):""}}')),
        TriggerTrait(
            name       = '',
            key        = afKey,
            actionKeys = [afKey+'Real',
                          updateDemorale+'Attacker',
                          updateType+'Attacker']),
        TriggerTrait(
            name       = '',
            key        = dfKey,
            actionKeys = [dfKey+'Real',
                          updateDemorale+'Defender',
                          updateType+'Defender',
                          updateWoods]),
        TriggerTrait(
            name       = '',
            command    = 'Print',
            key        = printKey,
            property   = f'{{{verbose}}}',
            actionKeys = []),
        ReportTrait(
            printKey,
            report = (f'{{{debug}?("~"+BasicName+" @ "+LocationName'
                      f'+" Pretty: "+PrettyName'
                      f'+" In woods: "+InWoods'
                      f'+" In houses: "+InHouses'
                      f'+" In Fortified: "+InFortified'
                      f'+" In Scenario: "+InScenario'
                      f'+" In turn: "+InTurn'
                      f'+" Unit turn: "+UnitTurn'
                      f'+" Unit hex: "+UnitHex'
                      f'+" Effective turn: "+{effectiveTurn}'
                      f'+" Exited: "+Exited'
                      f'+" Opponent demoralised: "+{otherDemorale}'
                      f'+" Scenario: "+Scenario'
                      f'+" CF: "+CF+" ("+EffectiveAF+")"'
                      f'+" DF: "+DF+" ("+EffectiveDF+")"'
                      f'+""):("!"+PrettyName+" @ "'
                      f'+LocationName.replace("@",""))}}')),
    ])
    battleUnitP.setTraits(*traits,basic)
    
    # ----------------------------------------------------------------
    # Battle calculation update with forest, fortress, overriver rules
    battleCalcP = prototypes[battleCalc]
    traits      = battleCalcP.getTraits()
    basic       = traits.pop()
    calcShift   = Trait.findTrait(traits,TriggerTrait.ID,
                                  key='key',         
                                  value=key(NONE,0)+',wgCalcBattleShift')
    shiftTram   = Trait.findTrait(traits,GlobalHotkeyTrait.ID,
                                  key='key',
                                  value=key(NONE,0)+',wgCalcBattleShiftTrampoline')
    shiftGlob   = Trait.findTrait(traits,GlobalPropertyTrait.ID,
                                  key='name',
                                  value='wgBattleShift')
    shiftProp   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsShift')
    oddsRProp   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsIndexRaw')
    oddsIProp   = Trait.findTrait(traits,CalculatedTrait.ID,
                                  key='name',value='OddsIndex')
    oddsCalc    = Trait.findTrait(traits,TriggerTrait.ID,
                                  key='key',
                                  value=key(NONE,0)+',wgCalcBattleOdds'
                                  )
    oddsRProp['expression'] = (f'{{'
                               f'(BattleFraction<.25)  ?1:' # 1:5
                               f'(BattleFraction<.3333)?2:' # 1:4
                               f'(BattleFraction<.5)   ?3:' # 1:3
                               f'(BattleFraction<1)    ?4:' # 1:2
                               f'(BattleFraction<2)    ?5:' # 1:1
                               f'(BattleFraction<3)    ?6:' # 2:1
                               f'(BattleFraction<4)    ?7:' # 3:1
                               f'(BattleFraction<5)    ?8:' # 4:1
                               f'(BattleFraction<6)    ?9:' # 5:1
                               f'10}}')
                               
    oddsIProp['expression'] = (f'{{Math.max(1,'
                               f'Math.min(OddsIndexRaw+OddsShift,10))}}')
    shiftProp['expression'] = (f'{{(({globalScenario}==3&&'
                               f'InfantryAttacker&&ArtilleryAttacker&&'
                               f'ReconnaissanceAttacker)?1:0)+'
                               f'(({globalScenario}!=4&&{attackerMorale})?-1:0)+'
                               f'(({globalScenario}!=4&&{defenderMorale})?1:0)+'
                               f'(({globalScenario}==4&&{defenderWoods})?-1:0)'
                               f'}}')
    rstActions = oddsCalc.getActionKeys()
    oddsCalc.setActionKeys([updateWoods+'Reset',updateDRM+'Reset']+
                           [updateDemorale+f+'Reset'
                            for f in ['Attacker','Defender']]+
                           [updateType+f+t+'Reset'
                            for f in ['Attacker', 'Defender']
                            for t in ['Infantry','Artillery',
                                      'Reconnaissance']]+
                           rstActions+
                           [updateDRM])

    shiftCmds = shiftGlob.getCommands()
    shiftCmds.append(['',shiftTram['key'],GlobalPropertyTrait.DIRECT,
                      '{OddsShift}'])
    shiftGlob.setCommands(shiftCmds)
    traits.remove(shiftTram)
    
    traits.extend([
        ReportTrait(
            key(NONE,0)+',wgCalcBattleShiftTrampoline',
            report = (f'{{{debug}?("! "+BasicName+" Calc battle shift:"'
                      f'+" Raw="+OddsIndexRaw'
                      f'+" Shift="+OddsShift'
                      f'+" InfantryAttacker="+InfantryAttacker'
                      f'+" ArtilleryAttacker="+ArtilleryAttacker'
                      f'+" ReconnaissanceAttacker="+ReconnaissanceAttacker'
                      f'+" InfantryDefender="+InfantryDefender'
                      f'+" ArtilleryDefender="+ArtilleryDefender'
                      f'+" ReconnaissanceDefender="+ReconnaissanceDefender'
                      f'+" AttackerDemorale="+{attackerMorale}'
                      f'+" DefenderDemorale="+{defenderMorale}'
                      f'+" DefenderWoods="+{defenderWoods}'
                      f'):""}}')),
        ReportTrait(
            key(NONE,0)+',wgCalcBattleFracReal',
            report = (f'{{{debug}?("~ "+BasicName+" Calc battle frac: "+'
                      f'"BattleFraction="+BattleFraction+'
                      f'" AF="+wgBattleAF+'
                      f'" DF="+wgBattleDF):""}}')),
        GlobalPropertyTrait(
            ['',updateWoods+'Reset',GlobalPropertyTrait.DIRECT,'{false}'],
            name = defenderWoods,
            numeric = False,
            description = 'Reset defender woods'),
        ReportTrait(
            updateWoods+'Reset',
            report = f'{{{debug}?("~ Reset defender in woods"):""}}'),
        GlobalPropertyTrait(
            ['',updateDRM+'Reset',GlobalPropertyTrait.DIRECT,'{0}'],
            ['',updateDRM,GlobalPropertyTrait.DIRECT,
             (f'{{({globalScenario}!=4)?0:(!Phase.contains("combat")?0:('
              f'ArtilleryDefender?-1:('
              f' ReconnaissanceDefender?('
              f'  ReconnaissanceAttacker?('
              f'   (InfantryDefender&&InfantryAttacker)?0:+1):-1):'
              f' (ReconnaissanceAttacker?+2:0))))}}')],
            name = battleDRM,
            numeric = False,
            description = 'Reset DRM'),
        ReportTrait(
            updateDRM+'Reset',
            report = f'{{{debug}?("~ Reset DRM"):""}}'),
        ReportTrait(
            updateDRM,
            report = (f'{{{debug}?("~ DRM: "+{battleDRM}'
                      f'+" Phase: "+Phase'
                      f'+" ("+(Phase.contains("combat"))+")"'
                      f'+" ReconnaissanceDefender: "+ReconnaissanceDefender' 
                      f'+" ReconnaissanceAttacker: "+ReconnaissanceAttacker' 
                      f'+" InfantryDefender: "+InfantryDefender' 
                      f'+" InfantryAttacker: "+InfantryAttacker' 
                      f'+" ArtilleryDefender: "+ArtilleryDefender' 
                      f'+" ArtilleryAttacker: "+ArtilleryAttacker' 
                      f'):""}}')),
    ])
    traits.extend([
        GlobalPropertyTrait(
            ['',updateDemorale+f+'Reset',GlobalPropertyTrait.DIRECT,'{false}'],
            name = f'{f}Demorale',
            numeric = False,
            description = f'Reset {f} demorale')
        for f in ['Attacker','Defender']])
    traits.extend([
        GlobalPropertyTrait(
            ['',updateType+f+t+'Reset',
             GlobalPropertyTrait.DIRECT,'{false}'],
            name = f'{t}{f}',
            numeric = False,
            description = f'Reset {f}{t}')
        for f in ['Attacker','Defender']
        for t in ['Infantry','Artillery','Reconnaissance']])
    traits.extend([
        ReportTrait(
            updateDemorale+f+'Reset',
            report = f'{{{debug}?("~ Reset {f} demorale"):""}}')
        for f in ['Attacker','Defender']])
    traits.extend([
        ReportTrait(
            updateType+f+t+'Reset',
            report = f'{{{debug}?("~ Reset {t}{f}: "+{t}{f}):""}}')
        for f in ['Attacker','Defender']
        for t in ['Infantry','Artillery','Reconnaissance']])
    
    # keys = calcShift.getActionKeys()
    # print(keys,calcShift['actionKeys'])
    # calcShift.setActionKeys(keys)
    battleCalcP.setTraits(*traits,basic)
        
    
    # ----------------------------------------------------------------
    # Modify faction prototypes 
    for faction in ['Allied', 'French']:
        other    = 'Allied' if faction == 'French' else 'French'
        factionp = prototypes[f'{faction} prototype']
        ftraits  = factionp.getTraits()
        fbasic   = ftraits.pop()
        
        # Remove traits we don't want nor need
        fdel                = Trait.findTrait(ftraits,DeleteTrait.ID)
        fdel['key']         = deleteKey
        fdel['name']        = ''
        feli                = Trait.findTrait(ftraits,SendtoTrait.ID,
                                              key='name',value='Eliminate')
        elik                = feli['key']
        feli['key']         = eliminateCmd
        feli['mapName']     = main['mapName']
        feli['boardName']   = f'{{{globalBoard}}}'
        feli['destination'] = SendtoTrait.ZONE
        feli['zone']        = f'{faction} dead'
        feli['x']           = 0
        feli['y']           = 0
        feli['name']        = ''
        feli['restoreName'] = ''
        feli['restoreKey']  = ''
        phmark              = faction
        scvar               = f'{faction}Schedule'
        # feli.print()
        ftraits.extend([
            MarkTrait(
                name       = pieceLayer,
                value      = unitLayer),
            MarkTrait(
                name       = 'Opponent',
                value      = other),
            CalculatedTrait(
                name        = f'{demorale}',
                expression  = f'{{{faction}Morale>1}}',
                description = 'If this faction has been demorialised'),
            CalculatedTrait(
                name        = f'{otherDemorale}',
                expression  = f'{{{other}Morale>1}}',
                description = 'If this faction has been demorialised'),
            CalculatedTrait(
                name        = 'InTurn',
                expression  = f'{{Phase.contains("{phmark}")}}',
                description = 'Unit in turn'),
            CalculatedTrait(
                name        = 'Exited',
                expression  = f'{{CurrentZone=="exit"&&Faction=="French"}}',
                description = 'Unit exited board'),
            CalculatedTrait(
                name        = 'Eliminated',
                expression  = f'{{CurrentZone=="{faction} dead"}}',
                description = 'Unit exited board'),
            RestrictCommandsTrait(
                name = 'Restrict eliminate',
                keys = [elik],
                hideOrDisable = RestrictCommandsTrait.DISABLE,
                expression = f'{{{notCombat}}}'),
            TriggerTrait(
                name = 'EliminateTrigger',
                command = 'Eliminate',
                key  = elik,
                actionKeys = [eliminateCmd,
                              incrLossKey,
                              storeMorale,
                              checkMorale,
                              recalcAll+'Check',
                              checkVictory]),
            TriggerTrait(
                name = 'RecalcTrigger',
                key  = recalcAll+'Check',
                property   = f'{{{faction}Morale==2&&{faction}Old==1}}',
                actionKeys = [recalcAll]),
            GlobalPropertyTrait(
                ['',incrLossKey,GlobalPropertyTrait.INCREMENT,'{CF}'],
                name = f'{faction}Loss',
                numeric = True,
                description = f'Increment {faction} CF loss'),
            ReportTrait(
                incrLossKey,
                report = (f'{{{debug}?("~ Increment {faction} CF loss by "+'
                          f'CF+" to "+{faction}Loss):""}}')),
            GlobalPropertyTrait(
                ['',storeMorale,GlobalPropertyTrait.DIRECT,
                 f'{{{faction}Morale}}'],
                name = f'{faction}Old',
                numeric = True,
                description = f'{faction} previous morale'),
            GlobalPropertyTrait(
                ['',checkMorale,GlobalPropertyTrait.DIRECT,
                 (f'{{{faction}Morale==1?('
                  f'({faction}Loss>={moraleLimit}&&{other}Morale==1)?2:1)'
                  f':2}}')],
                name = f'{faction}Morale',
                numeric = True,
                description = f'{faction} morale'),
            ReportTrait(
                checkMorale,
                report = (f'{{{debug}?("~ {faction} morale: "+'
                          f'{faction}Morale+" French exited="+'
                          f'{frenchExited}):""}}')),
            ReportTrait(
                checkMorale,
                report = (f'{{({faction}Morale>{faction}Old)?'
                          f'Alert("{faction} demoralised: "+'
                          f'{faction}Loss+" CFs lost"):""}}')), 
            GlobalHotkeyTrait(
                name         = '',
                key          = recalcAll,
                globalHotkey = recalcAll,
                description  = 'Recalculate all odds'),
            GlobalHotkeyTrait(
                name         = '',
                key          = checkVictory,
                globalHotkey = checkVictory,
                description  = 'Check for victory'),
        ])
        if faction == 'French':
            pass
        
        # ftraits.remove(fdel)
        factionp.setTraits(*ftraits,fbasic)

    # ================================================================
    #
    # Get all pieces and modify them
    #
    pieces  = game.getPieces(asdict=False)
    schedules = {'fr iv 12 id':  [4,5,6],
                 'fr iv 13 id':  [4,5,6],
                 'fr iv 14 id':  [4,5,6],
                 'fr iv ard':    [4,5,6],
                 'fr iic 9 cd':  [4,5,6],
                 'fr iii 8 id':  [6],
                 'fr iii 10 id': [6],
                 'fr iii 11 id': [6],
                 'fr iii ard':   [6],
                 'fr vi 21 id':  [6],
                 'fr iv 6 cd':   [6],
                 'fr iic 10 cd': [6],
                 'fr ic 4 cd':   [6],
                 #
                 'pr ii 5 ib':  [1,3,4,5,6],
                 'pr iv 13 ib': [1,3,4,5,6],
                 'pr ii c cd':  [1,3,4,5,6],
                 'pr iv arb':   [1,3,4,5,6],
                 'pr iv 14 ib': [1,3,5,6],
                 'pr iv 15 ib': [1,3,5,6],
                 'pr iv 16 ib': [1,3,5,6],
                 'pr iv c cd':  [1,3,5,6],
                 'pr ii arb':   [1,3,5,6],
                 #
                 'pr ii 6 ib':  [5,6],
                 'pr ii 7 ib':  [5,6],
                 'pr iii c cd': [5,6],
                 'pr i arb':    [5,6],
                 #
                 'pr i 1 ib':   [6],
                 'pr i 3 ib':   [6],
                 'pr i c cd':   [6]}
    mods = { 'br res arb':   { 'hex': '13@09' },
             'br res ha ib': { 'hex': '09@13' },
             #               
             'pr iv 13 ib':  { 'turn': 6 },
             'pr iv 14 ib':  { 'turn': 6 },
             'pr iv c cd':   { 'turn': 6 },
             'pr iv 15 ib':  { 'turn': 7 },
             'pr iv 16 ib':  { 'turn': 7 },
             'pr iv arb':    { 'turn': 7 },
             #               
             'pr ii 5 ib':   { 'turn': 8 },
             'pr ii 6 ib':   { 'turn': 8 },
             'pr ii 7 ib':   { 'turn': 8 },
             'pr ii c cd':   { 'turn': 8 },
             'pr ii arb':    { 'turn': 8 },
             'pr i 1 ib':    { 'turn': 8 },
             'pr i 3 ib':    { 'turn': 8 },
             'pr i c cd':    { 'turn': 8 },
             'pr i arb':     { 'turn': 8 },
             'pr iii c cd':  { 'turn': 8 },
             #
             'fr gd ard':    { 'hex': '14@15' },
             'fr i ard':     { 'hex': '15@14' },
             'fr ii ard':    { 'hex': '09@15' },
             'fr i 3 id':    { 'hex': '17@13' },
             'fr i 4 id':    { 'hex': '14@13' },
             #
             'fr iv 19 id':  { 'hex': None, 'turn': 3 },
             'fr iv 20 id':  { 'hex': None, 'turn': 3 },
             'fr iii 3 cd':  { 'hex': None, 'turn': 3 },
             'fr ic 5 cd':   { 'hex': None, 'turn': 3 },
             'fr vi ard':    { 'hex': None, 'turn': 3 },
            }

    alts = {'fr gd gren 1 ib': { 'hex': '15@17' },
            'fr gd gren 2 ib': { 'hex': '15@17' },
            'fr nap':          { 'hex': '15@17' } }

    redOdds = {'1:4': '1:5',
               '1:3': '1:4',
               '1:2': '1:3',
               '1:1': '1:2',
               '2:1': '1:1',
               '3:1': '2:1',
               '4:1': '3:1',
               '5:1': '4:1',
               '6:1': '5:1'}
    
    traits = None
    basic  = None
    for piece in pieces:
        name    = piece['entryName'].strip()
        faction = name[:3]
        Faction = 'French' if faction in ['fr '] else 'Allied'

        ptraits = piece.getTraits()
        pbasic  = ptraits.pop()

        # Remove errouneous prototype 
        dp = Trait.findTrait(ptraits,PrototypeTrait.ID,
                             key = 'name',
                             value = ' prototype')
        if dp:
            ptraits.remove(dp)
        
        if name == hidden:
            # print(f'Setting traits on {hidden}')
            checkAlliedVP   = key(NONE,0)+',checkAlliedVP'
            checkFrenchVP   = key(NONE,0)+',checkFrenchVP'
            checkDiffVP     = key(NONE,0)+',checkDiffVP'
            setAsUser       = key(NONE,0)+',setAsUser'
            resetAsUser     = key(NONE,0)+',resetAsUser'
            
            ptraits = [
                GlobalHotkeyTrait(
                    name         = '',
                    key          = optKey,
                    globalHotkey = optKey,
                    description  = 'Show optional rules'),
                GlobalPropertyTrait(
                    ['',
                     f'{initBoard}',GlobalPropertyTrait.DIRECT,
                     f'{{CurrentBoard}}'],
                    name        = f'{globalBoard}',
                    numeric     = False,
                    description = 'Update current board'),
                TriggerTrait(
                    name       = 'Set up stuff',
                    command    = '',
                    key        = setitupKey,
                    property   = '',
                    actionKeys = [scheduleKey+'French',
                                  scheduleKey+'Allied',
                                  updateMorale]),
                GlobalPropertyTrait(
                    ['',
                     f'{scheduleKey}French',GlobalPropertyTrait.DIRECT,
                     f'{{{globalScenario}==1?1:Random(6)}}'],
                    name        = f'FrenchSchedule',
                    numeric     = True,
                    description = 'Random french schedule'),
                GlobalPropertyTrait(
                    ['',
                     f'{scheduleKey}Allied',GlobalPropertyTrait.DIRECT,
                     f'{{{globalScenario}==1?1:Random(6)}}'],
                    name        = f'AlliedSchedule',
                    numeric     = True,
                    description = 'Random allied schedule'),
                ReportTrait(
                    initBoard,
                    report = (f'{{{debug}?("~Board: "+BasicName+" "+'
                              f'CurrentBoard+" "+{globalBoard}):""}}')),
                ReportTrait(
                    optKey,
                    report = (f'{{{debug}?("~Board: "+BasicName+" "+'
                              f'"show optionals window"):""}}')),
                # Below exposes players schedules!
                # ReportTrait(
                #     f'{scheduleKey}French', f'{scheduleKey}Allied',
                #     report = (f'{{{debug}?("~Schedules Allied="+'
                #               f'AlliedSchedule+" French="+'
                #               f'FrenchSchedule):""}}')),
                GlobalPropertyTrait(
                    ['',
                     updateMorale,
                     GlobalPropertyTrait.DIRECT,
                     f'{{{globalScenario}==4?90:40}}'],
                    name        = moraleLimit,
                    numeric     = True,
                    description = 'Set morale limit'),
                GlobalPropertyTrait(
                    ['',checkCombat,GlobalPropertyTrait.DIRECT,
                     f'{{Phase.contains("movement")}}'],
                    name        = notCombat,
                    numeric     = True,
                    description = 'Check for combat phase'),
                ReportTrait(
                    updateMorale,
                    report = (f'{{{debug}?("~ Morale limit is "+'
                              f'{moraleLimit}):""}}')),
                DynamicPropertyTrait(
                    ['',setAsUser,DynamicPropertyTrait.DIRECT,'{1}'],
                    ['',resetAsUser,DynamicPropertyTrait.DIRECT,'{0}'],
                    name = 'AsUser',
                    numeric = True,
                    value = 0),
                TriggerTrait(
                    name       = 'Check for victory, user initiated',
                    command    = '',
                    key        = checkVictory+'User',
                    actionKeys = [setAsUser,
                                  checkVictory,
                                  resetAsUser]),
                TriggerTrait(
                    name       = 'Check for victory',
                    command    = '',
                    key        = checkVictory,
                    property   = f'{{{globalScenario}<4}}',
                    actionKeys = [checkExit,
                                  checkSudden,
                                  showWinner]),
                TriggerTrait(
                    name       = 'Check for victory',
                    command    = '',
                    key        = checkVictory,
                    property   = f'{{{globalScenario}==4}}',
                    actionKeys = [checkExit,
                                  checkAlliedVP,
                                  checkFrenchVP,
                                  checkDiffVP,
                                  showWinner]),
                ReportTrait(
                    checkVictory,
                    report = (f'{{{debug}?("~ Check for victory "+'
                              f'{globalScenario}+" "+{globalVictor}):""}}')),
                GlobalPropertyTrait(
                    ['',checkExit,GlobalPropertyTrait.DIRECT,
                     (f'{{Count("{{Exited}}","{main["mapName"]}")}}')],
                    name = frenchExited,
                    numeric = True,
                    description = f'{faction} morale'),
                ReportTrait(
                    checkExit,
                    report = (f'{{{debug}?("~"+{frenchExited}+'
                              f'" French units has exited"):""}}')),
                GlobalPropertyTrait(
                    ['',checkSudden,GlobalPropertyTrait.DIRECT,
                     (f'{{'
                      f'FrenchMorale>1?"Allied victory":('
                      f'(AlliedMorale>1&&{frenchExited}>=7)?"French victory":'
                      f'"Tie")}}')],
                    name = globalVictor,
                    numeric = False,
                    description = 'Check for sudden death'),
                ReportTrait(
                    checkSudden,
                    report = (f'{{({debug}||AsUser>0)?((AsUser>0?"`":"~")'
                              f'+" French losses: "+FrenchLoss+","'
                              f'+" Allied losses: "+AlliedLoss+","'
                              f'+" French morale: "+(FrenchMorale==1?"good,":"bad,")'
                              f'+" Allied morale: "+(AlliedMorale==1?"good,":"bad,")'
                              f'+" French exited: "+{frenchExited}'
                              f'):""}}')),
                GlobalPropertyTrait(
                    ['',checkAlliedVP,GlobalPropertyTrait.DIRECT,
                     (f'{{'
                      f'(Count("{{BasicName==\\"fr nap\\"&&Eliminated}}")>0||'
                      f'FrenchMorale>1)?3:('
                      f'FrenchLoss>=100?2:(FrenchLoss>=75?1:0))}}')],
                    name = alliedVP,
                    numeric = True,
                    description = 'Allied victory points'),
                GlobalPropertyTrait(
                    ['',checkFrenchVP,GlobalPropertyTrait.DIRECT,
                     (f'{{'
                      f'(Count("{{BasicName==\\"fr nap\\"&&Exited}}")>0&&'
                      f'{frenchExited}>=11)?3:('
                      f'AlliedLoss>=150?2:(AlliedMorale>1?1:0))}}')],
                    name = frenchVP,
                    numeric = True,
                    description = 'French victory points'),
                GlobalPropertyTrait(
                    ['',checkDiffVP,GlobalPropertyTrait.DIRECT,
                     (f'{{(Math.abs({frenchVP}-{alliedVP})==3?"decisive ":'
                      f'   Math.abs({frenchVP}-{alliedVP})==2?"moderate ":'
                      f'   Math.abs({frenchVP}-{alliedVP})==1?"marginal ":"")+'
                      f'  (({frenchVP}>{alliedVP})?"French victory":'
                      f'   ({frenchVP}<{alliedVP})?"Allied victory":"Tie")}}')],
                    name = globalVictor,
                    numeric = False,
                    description = 'Check for advanced VP levels'),
                ReportTrait(
                    checkAlliedVP,
                    report = (f'{{({debug}||AsUser>0)?('
                              f'(AsUser>0?"`":"~")+"Allied VP "+{alliedVP}'
                              f'+" Napoleon: "+'
                              f'Count("{{BasicName==\\"fr nap\\"&&Eliminated}}")'
                              f'+" French morale: "+(FrenchMorale==1?"good":"bad")'
                              f'+" French losses: "+FrenchLoss'
                              f'):""}}')),
                ReportTrait(
                    checkFrenchVP,
                    report = (f'{{({debug}||AsUser>0)?('
                              f'(AsUser>0?"`":"~")+"French VP "+{frenchVP}'
                              f'+" Napoleon: "+'
                              f'Count("{{BasicName==\\"fr nap\\"&&Exited}}")'
                              f'+" French exited: "+{frenchExited}'
                              f'+" Allied morale: "+(AlliedMorale==1?"good":"bad")'
                              f'+" Allied losses: "+AlliedLoss'
                              f'):""}}')),
                ReportTrait(
                    checkDiffVP,
                    report = (f'{{({debug}||AsUser>0)?((AsUser?"`":"~")'
                              f'+"VP difference "'
                              f'+(Math.abs({frenchVP}-{alliedVP}))+" "'
                              f'+({frenchVP}>{alliedVP})+" -> "'
                              f'+{globalVictor}):""}}')),
                GlobalHotkeyTrait(
                    name         = '',
                    key          = showWinner,
                    globalHotkey = showWinner,
                    description  = 'Show possible winner'),
                ReportTrait(
                    showWinner,
                    report=(f'{{AsUser>0?("`The game is currently a "+{globalVictor}):""}}'))
            ]+ptraits

        elif 'odds marker' in name:
            # print(f'Modify odds marker "{name}"')

            odds = name.replace('odds marker','').strip()
            brow = basicCrt[odds]
            arow = advCrt  [odds]
            bcal = ':'.join([f'(Die=={n+1})?"{r}"'
                             for n,r in enumerate(brow)])+f':{brow[-1]}'
            acal = ':'.join([f'(Die=={n-1})?"{r}"'
                             for n,r in enumerate(arow)])+f':{arow[-1]}'
            ptraits.extend([
                MarkTrait(name='OddsName',value=odds),
                CalculatedTrait(
                    name = 'BasicResult',
                    expression = f'{{{bcal}}}',
                    description = f'Calculate basic result for odds {odds}'),
                CalculatedTrait(
                    name = 'AdvancedResult',
                    expression = f'{{{acal}}}',
                    description = f'Calculate advanced result for odds {odds}'),
            ])

            red = redOdds.get(odds,None)
            if red is not None:
                ered = red.replace(':',r'\:')
                skel = PlaceTrait.SKEL_PATH()
                path = skel.format('OddsMarkers',
                                   f'odds marker {ered}')
                # print(f'Odds reduction from {odds} to {red} -> {path}')

                ptraits.extend([
                    TriggerTrait(
                        name       = 'Reduce odds',
                        command    = 'Reduce',
                        key        = key('L'),
                        property   = '',
                        actionKeys = [
                            key(NONE,0)+',wgGetBattle',
                            key(NONE,0)+',wgSetBattle',
                            key(NONE,0)+',oddsReduce']),
                    ReplaceTrait(
                        command     = '',
                        key         = key(NONE,0)+',oddsReduce',
                        markerSpec  = path,
                        afterKey    = key(NONE,0)+',wgGetBattle',
                        gpid        = game.nextPieceSlotId(),
                        description = f'Reduce odds to {red}'),
                    ReportTrait(
                        key('L'),
                        report=(f'{{{verbose}?("!Reduce odds from '
                                f'{odds} to {red}"):""}}')),
                    ])

        elif name == 'scenario 1':
            # print(f'Scenario marker modify')
                        # Modify scenario selector piece
            step                 = Trait.findTrait(ptraits, LayerTrait.ID,
                                                   'name', 'Step')
            bky                  = key(NONE,0)+','+globalScenario
            imgs                 = step['images'].split(',')
            step['resetKey']     = ''
            step['resetName']    = ''
            step['increaseKey']  = ''
            step['increaseName'] = ''
            step['follow']       = True
            step['expression']   = f'{{{globalScenario}}}'
            step['images']       = ','.join([imgs[0],
                                             imgs[0].replace("1","2"),
                                             imgs[0].replace("1","3"),
                                             imgs[0].replace("1","4")])
            bkys                = [bky+f'{i+1}'
                                   for i,_ in enumerate(scenarios)]
            select              = [[n,k,GlobalPropertyTrait.DIRECT, f'{i+1}']
                                   for i,(k,n) in
                                   enumerate(zip(bkys,scenarios))]
            step['newNames']    = ','.join(scenarios)
            ptraits = [
                ClickTrait(
                    key         =  '',
                    context     = True,
                    whole       = True,
                    description = 'Select start-up'),
                RestrictCommandsTrait(
                    name          = 'Restrict scenario choice',
                    hideOrDisable = RestrictCommandsTrait.DISABLE,
                    expression    = f'{{Phase!="Setup"}}',
                    keys          = bkys),
                GlobalPropertyTrait(
                    *select,
                    name        = globalScenario,
                    numeric     = True,
                    wrap        = True,
                    description = 'Select scenario'),
                ReportTrait(*bkys,
                            report=(f'{{{debug}?("!Scenario: "+'
                                    f'{globalScenario}):""}}')),
                RestrictAccessTrait(sides=[],
                                    description='Cannot move'),
                step]

        elif name == gameTurn:
            # Game turn marker 
            markers  = Trait.findTrait(ptraits,
                                       PrototypeTrait.ID,
                                       key = 'name',
                                       value = 'Markers prototype')
            if markers is not None:
                ptraits.remove(markers)
            
            # Add the prototypes we created above 
            ptraits.extend([PrototypeTrait(pnn + ' prototype')
                            for pnn in turnp])
            ptraits.extend([
                RestrictAccessTrait(sides=[],
                                    description='Cannot move'),
                                                        
            ])
            # Must be set here, because we make copy below!
            piece.setTraits(*ptraits,pbasic)
            ptraits = None
            pcopy   = None
            
            cpy  = main.addAtStart(name            = f'{name}@turn 1',
                                   location        = 'Turn 1@turns',
                                   useGridLocation = True,
                                   ).addPiece(piece)

        elif 'morale' in name:
            # print(f'Morale marker modify')
            step               = Trait.findTrait(ptraits, LayerTrait.ID,
                                                 'name', 'Step')
            faction            = ('Allied' if name.startswith('br') else
                                  'French')
            step['follow']     = True
            step['expression'] = f'{{{faction}Morale}}'
            ptraits            = [
                step,
                RestrictAccessTrait(sides=[], description='Cannot move')]

        elif 'reinf q' in name:
            # print(f'Reinforcement schedule marker modify')
            base    = name[:2]+'_reinf_'
            faction = 'Allied' if name.startswith('br') else 'French'
            scvar   = f'{faction}Schedule'
            imgs    = [getImage(f'{base}{i}x') for i in range(1,7)]
            nams    = [f'Schedule {i}'   for i in range(1,7)]
            # print(imgs,nams)
            ptraits = [
                PrototypeTrait(name='Schedule prototype'),
                MaskTrait(
                    keyCommand   = hideReinf,
                    maskedName   = 'Reinforcement schedule',
                    hideCommand  = 'Mask',
                    access       = [faction,'<observer>'],
                    displayStyle = MaskTrait.PEEK,
                    imageName    = getImage(f'{base}q')),
                LayerTrait(
                    resetKey     = '',
                    resetName    = '',
                    increaseKey  = '',
                    increaseName = '',
                    follow       = True,
                    expression   = scvar,
                    images       = imgs,
                    #             newNames     = nams),
                    description  = 'Schedule layers'),
                ReportTrait(
                    hideReinf,
                    report = f'{{{debug}?("~Hiding "+BaseName):""}}'),
                #RestrictAccessTrait(sides=[], description='Cannot move'),
            ]
                
        elif faction in ['fr ','br ','pr ']:
            # Pretty name 
            pt = prettyName(name)
            ptraits.append(MarkTrait(name = 'PrettyName',
                                     value = pt.replace(', ',' - ')))
            #print(pt)
            #print(f'{name:<30s} {pt}')
            
            # Check if we have artillery and reconnaissance
            ar = Trait.findTrait(ptraits,PrototypeTrait.ID,
                                 key = 'name',
                                 value = 'artillery reconnaissance prototype')
            if ar is not None:
                # If so, we if we have the reconnaissance,
                # and the artillery 
                rp = Trait.findTrait(ptraits,PrototypeTrait.ID,
                                     key   = 'name',
                                     value = 'reconnaissance prototype')
                ap = Trait.findTrait(ptraits,PrototypeTrait.ID,
                                     key   = 'name',
                                     value = 'artillery prototype')
                # If we have reconnaissance, remove it 
                if rp is not None: ptraits.remove(rp)
                # If we have artillery, remove combined 
                if ap is not None: ptraits.remove(ar)

                
            
            hx = None
            st = None
            for trait in ptraits:
                if trait.ID == MarkTrait.ID:
                    if hx is None and trait['name'] == 'upper right':
                        hx = trait['value']
                        hx = sub('[^=]+=','',hx).strip()
                        hx = None if hx == '' else hx[:2]+'@'+hx[2:]
                    if trait['name'] == 'upper left':
                        st  = trait['value']
                        st  = sub('[^=]+=','',st).strip()
                        st  = None if st == '' else int(st)

            if st is None or int(st) == 0:
                st = 1


            pr = piece.getParent()
            bn = 'none'
            #print(pr,pr.__class__.__name__)
            if 'AtStart' in pr.__class__.__name__:
                bn = pr['owningBoard']

                if 'OOB' in bn:
                    advanced = 'Advanced' in bn
                    esdaile  = 'Esdaile'  in bn
                    basic    = not advanced and not esdaile
                    scen     = (f'{{{globalScenario}==4}}' if advanced else
                                f'{{{globalScenario}==3}}' if esdaile else
                                f'{{{globalScenario}<3}}')

                    if esdaile:
                        st = mods.get(name,{}).get('turn',st)
                        hx = mods.get(name,{}).get('hex', hx)
                    hx2 = None
                    if advanced:
                        hx2 = alts.get(name,{}).get('hex',None)

                    
                    ptraits.extend([
                        CalculatedTrait(
                            name        = 'InScenario',
                            expression  = scen,
                            description = 'Unit in scenario')])

                    sched = None
                    if basic:
                        sched = schedules.get(name,None)

                    if sched is not None:
                        scvar = (('French' if faction == 'fr' else 'Allied')
                                 + 'Schedule')
                        scexp = '||'.join([
                            f'{scvar}=={sc}' for sc in sched])
                        delay = 3 in sched
                        sctrn = (f'{{{scvar}==3?{st+2}:{st}}}' if delay
                                 else f'{{{st}}}')
                        ptraits.extend([
                            CalculatedTrait(
                                name        = 'InSchedule',
                                expression  = f'{{{scexp}}}',
                                description = 'Unit in current schedule'),
                            CalculatedTrait(
                                name        = 'UnitTurn',
                                expression  = sctrn,
                                description = 'Unit in current schedule')])
                    else:
                        ptraits.extend([
                            MarkTrait(
                                name = 'InSchedule',
                                value = 'true'),
                            MarkTrait(
                                name = 'UnitTurn',
                                value = str(st))])
                            
                    if hx is not None:
                        if hx2 is not None:
                            ptraits.extend([
                                TriggerTrait(
                                    name       = 'Setup',
                                    command    = '',
                                    key        = setupKey+'Real',
                                    property   = f'{{{globalBoard}=="Large"}}',
                                    actionKeys = [setupKey+'Large']),
                                TriggerTrait(
                                    name       = 'Setup',
                                    command    = '',
                                    key        = setupKey+'Real',
                                    property   = f'{{{globalBoard}=="Small"}}',
                                    actionKeys = [setupKey+'Small']),
                                SendtoTrait(mapName     = main['mapName'],
                                            boardName   = f'{{{globalBoard}}}',
                                            name        = '',
                                            restoreName = '',
                                            restoreKey  = '',
                                            zone        = 'hexes',
                                            destination = SendtoTrait.GRID,
                                            position    = hx,
                                            key         = setupKey+'Large',
                                            x           = 0,
                                            y           = 0,
                                            description = 'Move to hex'),
                                SendtoTrait(mapName     = main['mapName'],
                                            boardName   = f'{{{globalBoard}}}',
                                            name        = '',
                                            restoreName = '',
                                            restoreKey  = '',
                                            zone        = 'hexes',
                                            destination = SendtoTrait.GRID,
                                            position    = hx2,
                                            key         = setupKey+'Small',
                                            x           = 0,
                                            y           = 0,
                                            description = 'Move to hex'),
                                ReportTrait(
                                    setupKey+'Large',
                                    report = (f'{{{debug}?(BasicName+'
                                              f'" move to {hx}"):""}}')),
                                ReportTrait(
                                    setupKey+'Small',
                                    report = (f'{{{debug}?(BasicName+'
                                              f'" move to {hx2}"):""}}'))
                                ])
                        else:
                            ptraits.extend([
                                SendtoTrait(mapName     = main['mapName'],
                                            boardName   = f'{{{globalBoard}}}',
                                            name        = '',
                                            restoreName = '',
                                            restoreKey  = '',
                                            zone        = 'hexes',
                                            destination = SendtoTrait.GRID,
                                            position    = hx,
                                            key         = setupKey+'Real',
                                            x           = 0,
                                            y           = 0,
                                            description = 'Move to hex'),
                                ReportTrait(
                                    setupKey+'Real',
                                    report = (f'{{{debug}?("~"+BasicName+'
                                              f'" move to {hx}"):""}}'))
                        ])
                    else:
                        upmarch = 'fr' if faction == 'fr ' else 'pr'
                        ptraits.extend([
                            SendtoTrait(mapName     = main['mapName'],
                                        boardName   = f'{{{globalBoard}}}',
                                        name        = '', #Reinforce',
                                        restoreName = '',
                                        restoreKey  = '',
                                        zone        = f'{upmarch} upmarch',
                                        destination = SendtoTrait.ZONE,
                                        key         = reinforceKey+'Real',
                                        x           = 0,
                                        y           = 0,
                                        description = 'Move to upmarch'),
                            ReportTrait(
                                reinforceKey+'Real',
                                report = (f'{{{debug}?("~"+'
                                          f'BasicName+" move to "'
                                          f'+"{upmarch} upmarch"):""}}'))
                        ])

                    ptraits.append(
                        PrototypeTrait('Reinforce prototype'))

            #if st is not None:
            #    traits.append(MarkTrait(name='UnitTurn',value=st))
            if hx is not None:
                ptraits.append(MarkTrait(name='UnitHex',value=hx))

            #for t in traits:
            #    print(f' {t}')

        if ptraits is not None and pbasic is not None:
            piece.setTraits(*ptraits,pbasic)

    # ----------------------------------------------------------------
    # Remove some pieces from the piece window, as we've placed them
    # on the map, and there's no need for the players to pick up new
    # ones.
    pwindows = game.getPieceWindows()
    for pw in pwindows.values():
        # print(f'Piece window: {pw["name"]}')
        tab = pw.getTabs().get("Counters",None)
        if not tab:
            continue

        panels = tab.getPanels()
        toRemove = ['Allied','French','Markers','MoraleMarkers',
                    'ScenarioMarkers','ScheduleMarkers']
        for rem in toRemove:
            pan = panels.get(rem,None)
            if pan:
                tab.remove(pan)
        

#
# EOF
#

                              
