#
#
#
NAME		:= NapoleonAtWaterloo
VERSION		:= 1.0
VMOD_VERSION	:= 1.1.0
VMOD_TITLE	:= Napoleon at Waterloo
VMOD_RES	:= 150
TUTORIAL	:= Tutorial.vlog

DOCFILES	:= $(NAME).tex			\
		   front.tex			\
		   preface.tex			\
		   rules.tex			\
		   materials.tex		
DATAFILES	:= hexes.tex			\
		   oob.tex			\
		   chits.tex			\
		   tables.tex			
OTHER		:= 
STYFILES	:= naw.sty			\
		   commonwg.sty
BOARDS		:= splitboardlrgA4.pdf		\
		   splitboardsmlA4.pdf		\
		   setupbsc.pdf
LBOARDS		:= splitboardlrgLetter.pdf	\
		   splitboardsmlLetter.pdf	\
		   setupbsc.pdf
SIGNATURE	:= 20
PAPER		:= --a4paper
TARGETSA4	:= $(NAME)A4.pdf 		\
		   $(NAME)A4Booklet.pdf 	\
		   $(NAME)A3Booklet.pdf 	\
		   materialsA4.pdf		\
		   hexeslrg.pdf			\
		   hexessml.pdf			\
		   splitboardlrgA3.pdf		\
		   splitboardlrgA4.pdf		\
		   splitboardsmlA3.pdf		\
		   splitboardsmlA4.pdf			

TARGETSLETTER	:= $(NAME)Letter.pdf 		\
		   $(NAME)LetterBooklet.pdf 	\
		   materialsLetter.pdf		\
		   splitboardlrgTabloid.pdf	\
		   splitboardlrgLetter.pdf	\
		   splitboardsmlTabloid.pdf	\
		   splitboardsmlLetter.pdf

include Variables.mk
include Patterns.mk

all:	a4
a4:	$(TARGETSA4) vmod
letter:	$(TARGETSLETTER)
vmod:	$(NAME).vmod
mine:	a4 cover.pdf box.pdf

%lrg.pdf:%.tex $(STYFILES)
	@echo "LATEX   	$* (large)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*lrg $<  $(REDIR)

%sml.pdf:%.tex $(STYFILES)
	@echo "LATEX   	$* (small)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*sml $<  $(REDIR)

.imgs/chits.png:chits.png
	@echo "CONVERT	$<"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/hexes%.png:hexes%.png
	@echo "CONVERT	$<"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/tables.png:tables.png
	@echo "CONVERT	$<"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/front.png:front.png
	@echo "CONVERT	$<"
	$(MUTE)$(CONVERT) $< $(CONVERT_FLAGS) $@

.imgs/logo.png:logo.png
	@echo "CONVERT	$<"
	mv $< $@

.imgs/br-oob1.png:oobA4.pdf
	@echo "PDFTOCAIRO	$<"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -png $<
	@echo "CONVERT		$<"
	$(MUTE)$(CONVERT) oobA4-1.png $(CONVERT_FLAGS) .imgs/br-oob1.png
	$(MUTE)$(CONVERT) oobA4-3.png $(CONVERT_FLAGS) .imgs/fr-oob1.png
	$(MUTE)$(CONVERT) oobA4-5.png $(CONVERT_FLAGS) .imgs/br-oob2.png
	$(MUTE)$(CONVERT) oobA4-7.png $(CONVERT_FLAGS) .imgs/br-oob3.png

update-images:	.imgs/chits.png 	\
		.imgs/hexeslrg.png 	\
		.imgs/hexessml.png 	\
		.imgs/br-oob1.png 	\
		.imgs/tables.png	\
		.imgs/front.png		\
		.imgs/logo.png		

include Rules.mk


# These generate images that are common for all formats 
export.pdf:		export.tex	$(DATAFILES)	$(STYFILES) \
			hexeslrg.pdf hexessml.pdf

$(NAME)A4.pdf:		$(NAME)A4.aux
$(NAME)A4.aux:		$(DOCFILES) 	$(STYFILES) 	$(BOARDS)	
materialsA4.pdf:	materials.tex 	$(STYFILES) 	$(DATAFILES) $(BOARDS)
materialsKSA4.pdf:	materials.tex 	$(STYFILES) 	$(DATAFILES) $(BOARDS)
oobA4.pdf:		oob.tex		$(DATAFILES)	$(STYFILES)
chitsA4.pdf:		chits.tex       		$(STYFILES)
chitsKSA4.pdf:		chits.tex       		$(STYFILES)
tablesA4.pdf:		tables.tex	$(DATAFILES)	$(STYFILES)
frontA4.pdf:		front.tex	$(DATAFILES)	$(STYFILES) 	\
			setupbsc.pdf
frontKSA4.pdf:		front.tex	$(DATAFILES)	$(STYFILES) 	\
			setupbscKS.pdf

$(NAME)Letter.pdf:	$(NAME)Letter.aux
$(NAME)Letter.aux:	$(DOCFILES) 	$(STYFILES) 	$(LBOARDS)
materialsLetter.pdf:	materials.tex 	$(DATAFILES) 	$(STYFILES) $(LBOARDS)
materialsKSLetter.pdf:	materials.tex 	$(DATAFILES) 	$(STYFILES) $(LBOARDS)
oobLetter.pdf:		oob.tex		$(DATAFILES)	$(STYFILES)
chitsLetter.pdf:	chits.tex       		$(STYFILES)
chitsKSLetter.pdf:	chits.tex       		$(STYFILES)
tablesLetter.pdf:	tables.tex	$(DATAFILES)	$(STYFILES)

# Generic dependency

$(NAME)LetterBooklet.pdf:PAPER=--letterpaper
$(NAME)LetterBooklet.pdf:SIGNATURE=20

setup%.pdf:	setup%.tex
setupbsc.pdf:	setupbsc.tex hexessml.pdf
setupev.pdf:	setupev.tex  hexessml.pdf
setupadv.pdf:	setupadv.tex hexeslrg.pdf
setupbscKS.pdf:	setupbsc.tex hexessml.pdf
setupevKS.pdf:	setupev.tex  hexessml.pdf
setupadvKS.pdf:	setupadv.tex hexeslrg.pdf

include Docker.mk

docker-artifacts::
	cp $(NAME).vmod $(NAME)-A4-$(VERSION)/

.PRECIOUS:	export.pdf 			\
		$(NAME)Rules.pdf		\
		splitboardsmlA4.tex		\
		splitboardsmlA3.tex		\
		splitboardlrgA4.tex		\
		splitboardlrgA3.tex		\
		splitboardsmlLetter.tex		\
		splitboardsmlTabloid.tex	\
		splitboardlrgLetter.tex		\
		splitboardlrgTabloid.tex	\
		$(NAME).vtmp 
#
# EOF
#

