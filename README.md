# Napoleon at Waterloo

[[_TOC_]]

This is my remake of the wargame _Napoleon at Waterloo_. The original
game was published first in 1971 by Simulations Publications,
Inc. (SPI), and a revised version in 1978 also by SPI (scans of the
original materials can be found at [`spigames`][spigames] or
[`simpubs`][simpubs]). 

This rework contains 

- Board 
  - The _original_ 1971 map. 
  - The _revised_ 1978 map. 
  
  The original map was larger by one and three hex columns to the east
  and west, respectively, and 5 hex rows to the south.
  
- Counters 
  - Original 1971 (second edition corrected) counters at division (xx)
    level. 
  - _Grouchy_ variant 1978 counters (xx).
  - 1971 _advanced_ game counters at brigade level (x).
- Rules 
  - The original 1971 _basic game_ rules (the 1978 rules are identical
    to these). 
  - The 1978 _Grouchy variant_ rules. 
  - The 1971 _advanced game_ rules. 
  - The _Esdaile variant_ rules from [C.J. Esdaile's article][jams 12
    3 11-44] ([see below](#articles)).
  
  The rules have been combined into a whole, with clear indications as
  to what applies when. 
  
The document _does not_ contain a historical account of the battle.
Instead, refer to te excellent articles by [A.A. Nofi][moves 3 24-26]
and [C.J. Esdaile][jams 12 3 11-44] ([see below](#articles)).

## Game mechanics 

|              |                    |
|--------------|--------------------|
| Period       | Gunpowder          |
| Level        | Operational        |
| Hex scale    | 400m (437 yards)   |
| Unit scale   | division (xx)      |
| Turn scale   | 1 hours            |
| Unit density | medium             |
| # of turns   | 10                 |
| Complexity   | 1 of 10            |
| Solitaire    | 9 of 10            |

Features:
- Campaign
- Variants
- Extension

## About this rework 

This rework is entirely new.  All text and graphics is new and nothing
is copied verbatim from the original materials. 

I have restructured and rewritten the rules somewhat (the text is all
new), as well as added illustrations of the rules. 


## The files 

The distribution consists of the following files 

- [NapoleonAtWaterlooA4.pdf][] This is a single document that contains
  everything: The rules, charts, counters, and order of battle, and
  the larger and smaller board split into four and two parts,
  respectively. 
  
  This document is meant to be printed on a duplex A4 printer.  The
  charts, OOB charts, counters, and board will be on separate sheets
  and can be separated from the rules.
  
- [NapoleonAtWaterlooA4Booklet.pdf][] This document _only_ contains the
  rules.  It is meant to be printed on a duplex A4 printer with long
  edge binding.  This will produce 5 sheets which should be folded
  down the middle of the long edge and stabled to form an 20-page A5
  booklet of the rules.
  
- [NapoleonAtWaterlooA3Booklet.pdf][] As above, but designed to be
  printed in an A3 duplex printer.  This will give 5 A3 sheets to form
  a 20-page A4 booklet. 
  
- [splitboardlrgA3.pdf][] holds the 1st edition (larger) board on two
  sheets of A3 paper.  Print this and glue on to a sturdy piece of
  cardboard (1.5mm poster carton seems a good choice).  Note that one
  sheet should be cut at the crop marks and glued over the other
  sheet.  Take care to align the sheets.
  
- [splitboardlrgA4.pdf][] has the 1st edition (larger) board split
  over four A4 sheets of paper.  Cut out and glue on to sturdy
  cardboard.  Note that one sheet should be cut at the crop marks and
  glued over the other sheet.  Take care to align the sheets.
  
- [splitboardsmlA3.pdf][] holds the 2nd edition (smaller) board on a
  single sheet of A3 paper.  Print this and glue on to a sturdy piece
  of cardboard (1.5mm poster carton seems a good choice).
  
- [splitboardsmlA4.pdf][] has the 2nd edition (smaller) board split
  over two A4 sheets of paper.  Cut out and glue on to sturdy
  cardboard.  Note that one sheet should be cut at the crop marks and
  glued over the other sheet.  Take care to align the sheets.
  
- [materialsA4.pdf][] holds the charts, counters, and OOB.  It is
  meant to be printed on A4 paper.  Print and glue on to a relatively
  thick piece of cardboard (1.5mm or so) or the like.
    
If you only have access to US Letter (and possibly Tabloid) printer,
you can use the following files

| *A4 Series*                         | *Letter Series*                          |
| ------------------------------------|------------------------------------------|
| [NapoleonAtWaterlooA4.pdf][]        | [NapoleonAtWaterlooLetter.pdf][]  	     |
| [NapoleonAtWaterlooA4Booklet.pdf][] | [NapoleonAtWaterlooLetterBooklet.pdf][]  |
| [NapoleonAtWaterlooA3Booklet.pdf][] | [NapoleonAtWaterlooTabloidBooklet.pdf][] |
| [materialsA4.pdf][]	              | [materialsLetter.pdf][]	                 |
| [splitboardlrgA4.pdf][]	          | [splitboardlrgLetter.pdf][]	             |
| [splitboardlrgA3.pdf][]	          | [splitboardlrgTabloid.pdf][]	         |
| [splitboardsmlA4.pdf][]	          | [splitboardsmlLetter.pdf][]	             |
| [splitboardsmlA3.pdf][]	          | [splitboardsmlTabloid.pdf][]	         |


Download [artifacts.zip][] to get all files for both kinds of paper
formats. 

## VASSAL modules 

Also available is a [VASSAL](https://vassalengine.org) module

- [VMOD][NapoleonAtWaterloo.vmod]

The module is generated from the same sources as the Print'n'Play
documents, and contains the rules as an embedded PDF.  The modules
features

- Battle markers 
- Automatic odds markers 
- Automatic battle resolution
- Selection of board 
- Basic or Advanced game or variants
- Embedded rules 
- Moved and battle markers automatically cleared 
- Symbolic dice 
- Layers
- Tutorial

## Previews

![1st Ed. Board](.imgs/hexeslrg.png)
![2nd Ed. Board](.imgs/hexessml.png)
![Charts](.imgs/tables.png)
![Counters](.imgs/chits.png)
![Front](.imgs/front.png)
![Basic Allied OOB](.imgs/br-oob1.png)
![Basic and Advanced French OOB](.imgs/fr-oob1.png)
![Variant Allied and French OOB](.imgs/br-oob2.png)
![Advanced Allied OOB](.imgs/br-oob3.png)
![VASSAL module](.imgs/vassal.png)
![VASSAL detail](.imgs/vassal_detail.png)
![VASSAL advanced](.imgs/vassal_adv.png)
![Components](.imgs/photo_components.png)
![Setup](.imgs/photo_setup.png)

## Articles 

* Pazda, R., "Advanced Withdrawal", [_Moves_ **3** p14-15][moves 3
  14-15], 1972.
* Nofi, A.A., "Grouchy at Waterloo", [_Moves_ **3** p24-26][moves 3
  24-26], 1972.
* Simonson, R.A., "The Bias that Nobody Knows", [_Moves_ **3**
  p28,30][moves 3 28,30], 1972.
* Porter, D.L., "Suggested Rules Changes for NAW and Borodino",
  [_Moves_ **11** p18-19][moves 11 18-19], 1973.
* Smith, D., "The Bias that doesn't Exist", [_Moves_ **13** p13][moves
  13 13], 1974.
* Swierczek, S., "An Expanded Napoleon Expansion Kit", [_Moves_
  **13** p13][moves 13 13], 1974.
* Simonsen, R.A., "8,000 to 1: The Readers of Moves Play the Editor of
  Moves", [_Moves_ **28** p32-33][moves 28 32-33], 1976.
* Simonsen, R.A., "8000 to 1 Results: Say, How'd You Guys Like to Put
  Some Money on This Game?", [_Moves_ **30** p18-19][moves 30 18-19],
  1977.
* Scarbeck, J., "Enhanced Napoleon at Waterloo", [_Moves_ **53**
  p14-17][moves 53 14-17], 1980.
* Best, J.B., "Expanded Simplicity", [_Moves_ **57** p17-18][moves 57
  17-18], 1981.
* Gibson, R., "Improving the Basic Napoleon at Waterloo", [_Phoenix_
  **3** p3][phoenix 3 3], 1976.
* Booth, M., "Napoleon at Waterloo", [_Phoenix_ **8** p5][phoenix 8 5],
  1977.
* Gibson, R., "Napoleon at Waterloo Revisited", [_Phoenix_ **11**
  p8][phoenix 11 8], 1978.
* Esdaile, C.J., ``Napoleon at Waterloo'', [_Journal of Advanced
    Military Studies_ **Vol.12**, #3, p11-44][jams 12 3 11-44], 2021,
    [doi://10.21140/mcuj.20211202001].
* Esdaile, C.J., [_Wargaming Waterloo_][mcup_ww], 2023, Marine Corps
    University Press.

## Implementation 

The whole package is implemented in LaTeX using my package
[_wargame_](https://gitlab.com/wargames_tex/wargame_tex).  This
package, combined with the power of LaTeX, produces high-quality
documents, with vector graphics to ensure that everything scales.

[spigames]: https://www.spigames.net/rules_downloads.htm#N
[simpubs]: https://nextcloud.simpubs.org/s/PB3Q5eipPCoNLK5

[artifacts.zip]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/download?job=dist

[NapoleonAtWaterlooA4.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/NapoleonAtWaterlooA4.pdf?job=dist
[NapoleonAtWaterlooA4Booklet.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/NapoleonAtWaterlooA4Booklet.pdf?job=dist
[NapoleonAtWaterlooA3Booklet.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/NapoleonAtWaterlooA3Booklet.pdf?job=dist
[materialsA4.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/materialsA4.pdf?job=dist
[splitboardlrgA3.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/splitboardlrgA3.pdf?job=dist
[splitboardsmlA3.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/splitboardsmlA3.pdf?job=dist
[splitboardlrgA4.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/splitboarlrgdA4.pdf?job=dist
[splitboardsmlA4.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/splitboarsmldA4.pdf?job=dist
[NapoleonAtWaterloo.vmod]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-A4-master/NapoleonAtWaterloo.vmod?job=dist


[NapoleonAtWaterlooLetter.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/NapoleonAtWaterlooLetter.pdf?job=dist
[NapoleonAtWaterlooLetterBooklet.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/NapoleonAtWaterlooLetterBooklet.pdf?job=dist
[NapoleonAtWaterlooTabloidBooklet.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/NapoleonAtWaterlooTabloidBooklet.pdf?job=dist
[materialsLetter.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/materialsLetter.pdf?job=dist
[splitboardlrgTabloid.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/splitboardlrgTabloid.pdf?job=dist
[splitboardsmlTabloid.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/splitboardsmlTabloid.pdf?job=dist
[splitboardlrgLetter.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/splitboardlrgLetter.pdf?job=dist
[splitboardsmlLetter.pdf]: https://gitlab.com/wargames_tex/naw_tex/-/jobs/artifacts/master/file/NapoleonAtWaterloo-Letter-master/splitboardsmlLetter.pdf?job=dist

[moves 3 14-15]: https://strategyandtacticspress.com/library-files/Moves%20Issue03.pdf#page=14
[moves 3 24-26]: https://strategyandtacticspress.com/library-files/Moves%20Issue03.pdf#page=24
[moves 3 28,30]: https://strategyandtacticspress.com/library-files/Moves%20Issue03.pdf#page=28
[moves 11 18-19]: https://strategyandtacticspress.com/library-files/Moves%20Issue11.pdf#page=18
[moves 13 13]: https://strategyandtacticspress.com/library-files/Moves%20Issue13.pdf#page=13
[moves 13 13]: https://strategyandtacticspress.com/library-files/Moves%20Issue13.pdf#page=14
[moves 28 32-33]: https://strategyandtacticspress.com/library-files/Moves%20Issue28.pdf#page=32
[moves 30 18-19]: https://strategyandtacticspress.com/library-files/Moves%20Issue30.pdf#page=18
[moves 53 14-17]: https://strategyandtacticspress.com/library-files/Moves%20Issue53.pdf#page=14
[moves 57 17-18]: https://strategyandtacticspress.com/library-files/Moves%20Issue57.pdf#page=17
[phoenix 3 3]: https://www.spigames.net/Phoenix/Phoenix3.pdf#page=3
[phoenix 8 5]: https://www.spigames.net/Phoenix/Phoenix8.pdf#page=5
[phoenix 11 8]: https://www.spigames.net/Phoenix/Phoenix11.pdf#page=8
[jams 12 3 11-44]: https://www.usmcu.edu/Portals/218/1_JAMS_12_2_Esdaile.pdf
[doi://10.21140/mcuj.20211202001]: https://doi.org/10.21140/mcuj.20211202001
[mcup_ww]: https://www.usmcu.edu/Outreach/Marine-Corps-University-Press/Books-by-topic/MCUP-Titles-A-Z/Wargaming-Waterloo/



