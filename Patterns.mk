# -*- Makefile -*-
#
# Various pattern rules.  Required variables
#
# - REDIR
# - MUTE
# - LATEX
# - LATEX_FLAGS
# - PDFJAM
# - PAPER
# - SIGNATURE
# - PDFTOCAIRO
# - CAIROFLAGS
#
# --- Rules for plain PDFs -------------------------------------------
%.pdf:	%.tex
	@echo "LATEX   	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.aux:	%.tex
	@echo "LATEX(1)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

%.pdf:	%.aux
	@echo "LATEX(2)	$*"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $* $(REDIR)

# --- Rules for A* PDFs ----------------------------------------------
%A4.aux:%.tex
	@echo "LATEX(1)	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%A4.aux
	@echo "LATEX(2)	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A4.pdf:%.tex
	@echo "LATEX   	$* (A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A4 $* $(REDIR)

%A3.pdf:%.tex
	@echo "LATEX   	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.aux:%.tex
	@echo "LATEX(1)	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

%A3.pdf:%A3.aux
	@echo "LATEX(2)	$* (A3)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*A3 $* $(REDIR)

# --- Rules for Letter-series PDFs -----------------------------------
%Letter.pdf:	%.tex
	@echo "LATEX   	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.aux:	%.tex
	@echo "LATEX(1)	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Letter.pdf:	%Letter.aux
	@echo "LATEX(2)	$* (Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Letter $* $(REDIR)

%Tabloid.pdf:	%.tex
	@echo "LATEX   	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.aux:	%.tex
	@echo "LATEX(1)	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

%Tabloid.pdf:	%Tabloid.aux
	@echo "LATEX(2)	$* (Tabloid)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*Tabloid $* $(REDIR)

# --- Rules for kriegspiel variants ----------------------------------
%KSA4.aux:	%.tex
	@echo "LATEX(1)	$* (Kriegspiel,A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*KSA4 $* $(REDIR)

%KSLetter.aux:	%.tex
	@echo "LATEX(1)	$* (Kriegspiel,Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*KSLetter $* $(REDIR)

%KSA4.pdf:	%.tex
	@echo "LATEX(1)	$* (Kriegspiel,A4)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*KSA4 $* $(REDIR)

%KSLetter.pdf:	%.tex
	@echo "LATEX	$* (Kriegspiel,Letter)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*KSLetter $* $(REDIR)

%KS.pdf:	%.tex
	@echo "LATEX	$* (Kriegspiel)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname $*KS $* $(REDIR)


# --- Rules for rules-only PDFs --------------------------------------
%Rules.pdf:%A4.pdf
	@echo "PDFJAM		$* (Rules)"
	$(MUTE)$(PDFJAM) $(PDFJAM_FLAGS) -o $@ -- $< "1-$(SIGNATURE)" $(REDIR)

# --- Rules for booklets ------------------------------------------
%Booklet.pdf:%.pdf
	@echo "BOOKLET 	$*"
	$(MUTE)$(PDFJAM) 			\
		--landscape 			\
		--suffix book 			\
		--signature '$(SIGNATURE)' 	\
		$(PAPER) 			\
		--outfile $@ 			\
		-- $< '1-$(SIGNATURE)' $(REDIR)

%A3Booklet.pdf:   %A4Booklet.pdf
	@echo "BOOKLET 	$* (A3)"
	$(MUTE)$(PDFJAM) 			\
		--a3paper 			\
		--landscape 			\
		--outfile $@ 			\
		-- $< $(REDIR)

%TabloidBooklet.pdf:   %LetterBooklet.pdf
	@echo "BOOKLET 	$* (Tabloid)*"
	$(MUTE)$(PDFJAM) 			\
		--scale 1.2 			\
		--papersize '{11in,17in}' 	\
		--landscape 			\
		--outfile $@ 			\
		--no-tidy			\
		-- $<  2>/dev/stdout | tee $@.log 2>&1 ;		\
	if test ! -f $@ ; then \
		l=`grep "a.log" $@.log` ; 	\
		echo "=== No-tidy log file is '$$l' ===" ; \
		if test "x$$l" != "x" ; then 	\
			echo "=== Start of $$l ===" ; \
			cat $$l ; \
			echo "=== End of $$l ===" ; \
			t=`dirname $$l`/`basename $$l .log`.tex ; \
			echo "=== Start of $$t ===" ; \
			cat $$t ; \
			echo "=== End of $$t ==="; fi ; false ; fi

# $(REDIR)

# --- Rules of calculating board splits ------------------------------
splitboard%.pdf:splitboard%.tex
	@echo "LATEX   	splitboard ($*)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) $< $(REDIR)

splitboardlrg%.tex:calcsplitlrg.tex hexeslrg.pdf $(STYLES)
	@echo "LATEX(0)	calcsplitlrg ($*)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname calcsplitlrg$* $< $(REDIR)

splitboardsml%.tex:calcsplitsml.tex hexessml.pdf $(STYLES)
	@echo "LATEX(0)	calcsplitsml ($*)"
	$(MUTE)$(LATEX) $(LATEX_FLAGS) -jobname calcsplitsml$* $< $(REDIR)


# --- Rules for images -----------------------------------------------
%.png:%-pdfjam.pdf
	@echo "PDFTOCAIRO	$* (via PDFJam)"
	$(MUTE)rm -f $@
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $* $(REDIR)

%.png:%.pdf
	@echo "PDFTOCAIRO	$*"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

%.png:%A4.pdf
	@echo "PDFTOCAIRO	$*"
	$(MUTE)$(PDFTOCAIRO) $(CAIROFLAGS) -singlefile -png $< $*

# --- Rules for VASSAL -----------------------------------------------
%.vtmp: %Rules.pdf export.pdf export.json $(TUTORIAL)
	@echo "VMOD		$@"
	$(MUTE)$(EXPORT) export.pdf export.json			\
		-r $(NAME)Rules.pdf				\
		-v $(VMOD_VERSION)				\
		-t "$(VMOD_TITLE)"				\
		-d "$(VMOD_DESC)"				\
		-S $(VMOD_SCALE)				\
		-R $(VMOD_RES)					\
		$(TUTORIAL:%.vlog=-T %.vlog)			\
		-o $@  $(EXPORT_FLAGS) $(REDIR)

%.vmod:	%.vtmp patch.py 
	@echo "VMOD    	$@ (patch)"
	@cp $< $@
	$(MUTE)$(EXPORT) patch $@ patch.py

#
# EOF
#
